package com.shop.service;

import com.shop.entity.product.description.Category;
import com.shop.repository.product.description.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category getCategory(Long categoryId) {
        return categoryRepository.findSubCategoriesById(categoryId);
    }

    public Set<Category> getCategories() {
        return categoryRepository.findAllByOrderByPriorityAsc();
    }

    public Set<Category> getFullCategories() {
        return categoryRepository.getAllByOrderByPriorityAsc();
    }
}
