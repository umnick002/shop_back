package com.shop.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.shop.entity.Status;
import com.shop.entity.customer.Customer;
import com.shop.entity.customer.CustomerAddress;
import com.shop.entity.customer.CustomerOrder;
import com.shop.entity.delivery.DeliveryPoint;
import com.shop.entity.delivery.DeliveryZone;
import com.shop.entity.delivery.DeliveryZoneTime;
import com.shop.entity.user.UserData;
import com.shop.repository.customer.CustomerOrderRepository;
import com.shop.repository.delivery.DeliveryPointRepository;
import com.shop.repository.delivery.DeliveryZoneTimeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

import static com.shop.config.Constant.*;
import static java.time.temporal.ChronoUnit.*;

@Service
public class DeliveryService {

    private static Logger LOGGER = LoggerFactory.getLogger(DeliveryService.class);

    private final DeliveryPointRepository deliveryPointRepository;
    private final DeliveryZoneTimeRepository deliveryZoneTimeRepository;
    private final CustomerService customerService;
    private final CustomerOrderService customerOrderService;
    private final CustomerOrderRepository customerOrderRepository;
    private final HttpRequest.Builder requestBuilder;
    private final HttpClient httpClient;

    @Autowired
    public DeliveryService(DeliveryPointRepository deliveryPointRepository,
                           HttpRequest.Builder requestBuilder, HttpClient httpClient,
                           DeliveryZoneTimeRepository deliveryZoneTimeRepository,
                           CustomerService customerService,
                           CustomerOrderService customerOrderService,
                           CustomerOrderRepository customerOrderRepository) {
        this.deliveryPointRepository = deliveryPointRepository;
        this.deliveryZoneTimeRepository = deliveryZoneTimeRepository;
        this.customerService = customerService;
        this.customerOrderService = customerOrderService;
        this.requestBuilder = requestBuilder;
        this.httpClient = httpClient;
        this.customerOrderRepository = customerOrderRepository;
    }

    public List<LocalDate> getDeliveryDays(UserData user, Long addressId) throws IllegalArgumentException {
        CustomerAddress customerAddress = customerService.getCustomerAddress(customerService.getCustomer(user), addressId, true);
        if (customerAddress == null) {
            throw new IllegalArgumentException(String.format("Address not found for user [%s]", user.getUsername()));
        }
        Map<LocalDate, Double> durations = buildDefaultDurations(customerAddress);
        Set<DeliveryPoint> deliveryPoints = getDeliveryPoints(durations.keySet());
        fillDurations(deliveryPoints, durations);
        int maxDuration = getSumDurationByZoneTimes(customerAddress.getDeliveryZone().getDeliveryZoneTimes());
        return getAvailableDeliveryDays(durations, maxDuration);
    }

    private List<LocalDate> getAvailableDeliveryDays(Map<LocalDate, Double> durations, int deliveryDuration) {
        List<LocalDate> deliveryDate = new ArrayList<>();
        for (LocalDate date : durations.keySet()) {
            if (durations.get(date) < (deliveryDuration * DELIVERY_LOAD)) {
                deliveryDate.add(date);
            }
        }
        deliveryDate.sort(Comparator.naturalOrder());
        return deliveryDate;
    }

    private Set<DeliveryPoint> getDeliveryPoints(Set<LocalDate> days) {
        return deliveryPointRepository.
                findAllByDeliveryDayInAndCustomerOrder_StatusNotInAndDeliveryZoneTimeIsNotNull(
                        days, Set.of(Status.Customer.DRAFT, Status.Customer.CANCEL));
    }

    private void fillDurations(Set<DeliveryPoint> deliveryPoints, Map<LocalDate, Double> durations) {
        for (DeliveryPoint deliveryPoint : deliveryPoints) {
            durations.put(deliveryPoint.getDeliveryDay(),
                    durations.get(deliveryPoint.getDeliveryDay()) + deliveryPoint.getDuration() + DELIVERY_WAIT);
        }
    }

    private int getSumDurationByZoneTimes(Set<DeliveryZoneTime> deliveryZoneTimes) {
        int duration = 0;
        for (DeliveryZoneTime deliveryZoneTime : deliveryZoneTimes) {
            duration += deliveryZoneTime.getAfter().until(deliveryZoneTime.getBefore(), SECONDS) - DELIVERY_TO_STORE;
        }
        return duration;
    }

    private Map<LocalDate, Double> buildDefaultDurations(CustomerAddress customerAddress) {
        Set<LocalDate> deliveryDays = getDeliveryDaysByZone(customerAddress.getDeliveryZone());
        Map<LocalDate, Double> durations = new HashMap<>();
        for (LocalDate deliveryDay : deliveryDays) {
            durations.put(deliveryDay, 0d);
        }
        return durations;
    }

    private Set<LocalDate> getDeliveryDaysByZone(DeliveryZone deliveryZone) {
        Set<LocalDate> deliveryDays = new HashSet<>();
        LocalDateTime now = LocalDateTime.now();
        Set<DayOfWeek> deliveryDaysOfWeek = deliveryZone.getDays();
        for (int i = 0; i < 5; i++) {
            if (deliveryDaysOfWeek.contains(now.getDayOfWeek())) {
                if (i == 0) {
                    DeliveryZoneTime deliveryZoneTime = deliveryZone.getDeliveryZoneTimes().iterator().next();
                    if (now.toLocalDate().atTime(deliveryZoneTime.getAfter()).isAfter(
                            now.plus(MINUTES.between(deliveryZoneTime.getAfter(), deliveryZoneTime.getBefore()), MINUTES))) {
                        deliveryDays.add(now.toLocalDate());
                    }
                } else {
                    deliveryDays.add(now.toLocalDate());
                }
            }
            now = now.plusDays(1);
        }
        return deliveryDays;
    }

    public List<DeliveryZoneTime> getDeliveryTimes(UserData user, Long addressId, String dateStr) {
        CustomerAddress customerAddress = customerService.getCustomerAddress(customerService.getCustomer(user), addressId, true);
        if (customerAddress == null) {
            return null;
        }
        LocalDate day = LocalDate.parse(dateStr);
        Map<DeliveryZoneTime, Set<DeliveryPoint>> deliveryPointsByTime =
                getDeliveryPointsByTimes(day, customerAddress.getDeliveryZone().getDeliveryZoneTimes());
        fillAvailability(deliveryPointsByTime, customerAddress);
        List<DeliveryZoneTime> deliveryZoneTimes = new ArrayList<>(deliveryPointsByTime.keySet());
        deliveryZoneTimes.sort(Comparator.comparing(DeliveryZoneTime::getAfter));
        return deliveryZoneTimes;
    }

    private void fillAvailability(Map<DeliveryZoneTime, Set<DeliveryPoint>> deliveryPointsByTime, CustomerAddress customerAddress) {
        for (DeliveryZoneTime deliveryZoneTime : deliveryPointsByTime.keySet()) {
            if (deliveryPointsByTime.get(deliveryZoneTime).size() > 0) {
                double oldDuration = 0d;
                for (DeliveryPoint deliveryPoint : deliveryPointsByTime.get(deliveryZoneTime)) {
                    oldDuration += deliveryPoint.getDuration() + DELIVERY_WAIT;
                }
                StringBuilder routeCoords = new StringBuilder();
                addLonLat(routeCoords, deliveryZoneTime.getDeliveryZone().getStock().getLon(),
                        deliveryZoneTime.getDeliveryZone().getStock().getLat());
                addLonLat(routeCoords, customerAddress.getLon(), customerAddress.getLat());
                fillOSRMPath(routeCoords, deliveryPointsByTime.get(deliveryZoneTime));
                addLonLat(routeCoords, deliveryZoneTime.getDeliveryZone().getStock().getLon(),
                        deliveryZoneTime.getDeliveryZone().getStock().getLat());
                try {
                    fetchRoute(routeCoords.toString());
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
//                deliveryZoneTime.setLoad(oldDuration);
            }
        }
    }

    private void fillOSRMPath(StringBuilder routeCoords, Set<DeliveryPoint> deliveryPoints) {
        for (DeliveryPoint deliveryPoint : deliveryPoints) {
            addLonLat(routeCoords, deliveryPoint.getCustomerOrder().getAddress().getLon(),
                    deliveryPoint.getCustomerOrder().getAddress().getLat());
        }
    }

    private Map<DeliveryZoneTime, Set<DeliveryPoint>> getDeliveryPointsByTimes(LocalDate day, Set<DeliveryZoneTime> deliveryZoneTimes) {
        Set<DeliveryPoint> deliveryPoints = deliveryPointRepository.findAllByDeliveryDayAndCustomerOrder_StatusNotInAndDeliveryZoneTimeIsNotNull(
                day, Set.of(Status.Customer.DRAFT, Status.Customer.CANCEL));
        Map<DeliveryZoneTime, Set<DeliveryPoint>> deliveryPointsByTime = new HashMap<>();
        LocalDateTime now = LocalDateTime.now();
        for (DeliveryZoneTime deliveryZoneTime : deliveryZoneTimes) {
            if (day.isAfter(now.toLocalDate()) || day.atTime(deliveryZoneTime.getAfter()).isAfter(
                    now.plus(MINUTES.between(deliveryZoneTime.getAfter(), deliveryZoneTime.getBefore()), MINUTES))) {
                deliveryPointsByTime.put(deliveryZoneTime, new HashSet<>());
            }
        }
        for (DeliveryPoint deliveryPoint : deliveryPoints) {
            if (deliveryPointsByTime.containsKey(deliveryPoint.getDeliveryZoneTime())) {
                deliveryPointsByTime.get(deliveryPoint.getDeliveryZoneTime()).add(deliveryPoint);
            }
        }
        return deliveryPointsByTime;
    }

    public DeliveryPoint createDeliveryPoint(UserData user, Long addressId, String dateStr, String afterStr, String beforeStr) {
        Customer customer = customerService.getCustomer(user);
        CustomerAddress customerAddress = customerService.getCustomerAddress(customer, addressId, false);
        if (customerAddress == null) {
            return null;
        }
        CustomerOrder customerOrder = customerOrderService.getTopByCustomerAndStatus(customer, Status.Customer.DRAFT);
        DeliveryPoint deliveryPoint;
        if (customerOrder.getDeliveryPoint() == null) {
            deliveryPoint = new DeliveryPoint();
            deliveryPoint.setCustomerOrder(customerOrder);
        } else {
            deliveryPoint = customerOrder.getDeliveryPoint();
        }
        deliveryPoint.setDeliveryDay(LocalDate.parse(dateStr));
        DeliveryZoneTime deliveryZoneTime = deliveryZoneTimeRepository.findByDeliveryZoneAndAfterAndBefore(
                customerOrder.getAddress().getDeliveryZone(),
                LocalTime.parse(afterStr), LocalTime.parse(beforeStr));
        deliveryPoint.setDeliveryZoneTime(deliveryZoneTime);
        return deliveryPointRepository.save(deliveryPoint);
    }

    private void addLonLat(StringBuilder builder, double lon, double lat) {
        builder.append(lon);
        builder.append(",");
        builder.append(lat);
        builder.append(";");
    }

    @Transactional
    public CustomerOrder confirmOrder(UserData user) {
        Customer customer = customerService.getCustomer(user);
        if (customer == null) {
            return null;
        }
        CustomerOrder customerOrder = customerOrderRepository.getByCustomerAndStatus(customer, Status.Customer.DRAFT);
        customerOrder.setStatus(Status.Customer.ORDER);
        Set<DeliveryPoint> deliveryPoints = addToRoute(customerOrder.getDeliveryPoint());
        deliveryPointRepository.saveAll(deliveryPoints);
        customerOrderRepository.save(customerOrder);
        return null;
    }

    private JsonNode fetchRoute(String path) throws IOException, InterruptedException, URISyntaxException {
        HttpRequest request = requestBuilder
                .uri(new URI(String.format("http://localhost:5000/trip/v1/car/%s?source=first&destination=last&annotations=duration",
                        path))
                ).GET().build();
        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        return OBJECT_MAPPER.readTree(response.body());
    }

    private Set<DeliveryPoint> addToRoute(DeliveryPoint deliveryPoint) {
        Set<DeliveryPoint> deliveryPoints = deliveryPointRepository.findAllByDeliveryDayAndDeliveryZoneTimeAndCustomerOrder_StatusNotIn(
                deliveryPoint.getDeliveryDay(), deliveryPoint.getDeliveryZoneTime(), Set.of(Status.Customer.DRAFT, Status.Customer.CANCEL));
        deliveryPoints.add(deliveryPoint);
        StringBuilder routeCoords = new StringBuilder();
        addLonLat(routeCoords, deliveryPoint.getCustomerOrder().getStock().getLon(), deliveryPoint.getCustomerOrder().getStock().getLat());
        fillOSRMPath(routeCoords, deliveryPoints);
        addLonLat(routeCoords, deliveryPoint.getCustomerOrder().getStock().getLon(), deliveryPoint.getCustomerOrder().getStock().getLat());
        try {
            JsonNode route = fetchRoute(routeCoords.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

//    private Load getLoad() {
//
//    }
//

}
