package com.shop.service;

import com.shop.entity.user.PersistentLogin;
import com.shop.repository.user.PersistentLoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

import static java.time.ZoneOffset.UTC;

@Repository
public class PersistentTokenService implements PersistentTokenRepository {

    private final PersistentLoginRepository persistentLoginRepository;

    @Autowired
    public PersistentTokenService(PersistentLoginRepository persistentLoginRepository) {
        this.persistentLoginRepository = persistentLoginRepository;
    }

    @Override
    public void createNewToken(PersistentRememberMeToken token) {
        PersistentLogin persistentLogin = new PersistentLogin(token.getUsername(), token.getSeries(),
                token.getTokenValue(), LocalDateTime.ofInstant(token.getDate().toInstant(), ZoneId.from(UTC)));
        persistentLoginRepository.save(persistentLogin);
    }

    @Override
    public void updateToken(String series, String tokenValue, Date lastUsed) {
        Optional<PersistentLogin> persistentLoginOptional = persistentLoginRepository.findById(series);
        if (persistentLoginOptional.isPresent()) {
            PersistentLogin persistentLogin = persistentLoginOptional.get();
            persistentLogin.setToken(tokenValue);
            persistentLogin.setLastUsed(LocalDateTime.ofInstant(lastUsed.toInstant(), ZoneId.from(UTC)));
            persistentLoginRepository.save(persistentLogin);
        }
    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(String seriesId) {
        Optional<PersistentLogin> persistentLoginOptional = persistentLoginRepository.findById(seriesId);
        if (persistentLoginOptional.isPresent()) {
            PersistentLogin persistentLogin = persistentLoginOptional.get();
            return new PersistentRememberMeToken(persistentLogin.getUsername(), persistentLogin.getSeries(),
                    persistentLogin.getToken(),
                    Date.from(persistentLogin.getLastUsed().atZone(ZoneId.from(UTC)).toInstant()));
        }
        return null;
    }

    @Override
    @Transactional
    public void removeUserTokens(String username) {
        persistentLoginRepository.deleteByUsername(username);
    }
}
