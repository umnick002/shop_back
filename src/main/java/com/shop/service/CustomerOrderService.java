package com.shop.service;

import com.shop.entity.Status;
import com.shop.entity.customer.Customer;
import com.shop.entity.customer.CustomerOrder;
import com.shop.entity.customer.CustomerOrderItem;
import com.shop.entity.product.price.SellPrice;
import com.shop.entity.stock.Stock;
import com.shop.entity.stock.StockReserve;
import com.shop.entity.user.UserData;
import com.shop.repository.customer.CustomerOrderItemRepository;
import com.shop.repository.customer.CustomerOrderRepository;
import com.shop.repository.customer.CustomerRepository;
import com.shop.repository.product.price.SellPriceRepository;
import com.shop.repository.stock.StockReserveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.time.ZoneOffset.UTC;

@Service
public class CustomerOrderService {

    private final CustomerRepository customerRepository;
    private final CustomerOrderRepository customerOrderRepository;
    private final CustomerOrderItemRepository customerOrderItemRepository;
    private final SellPriceRepository sellPriceRepository;
    private final StockReserveRepository stockReserveRepository;

    @Autowired
    public CustomerOrderService(CustomerRepository customerRepository, CustomerOrderRepository customerOrderRepository,
                                CustomerOrderItemRepository customerOrderItemRepository,
                                SellPriceRepository sellPriceRepository, StockReserveRepository stockReserveRepository) {
        this.customerRepository = customerRepository;
        this.customerOrderRepository = customerOrderRepository;
        this.customerOrderItemRepository = customerOrderItemRepository;
        this.sellPriceRepository = sellPriceRepository;
        this.stockReserveRepository = stockReserveRepository;
    }

    public CustomerOrder getTopByCustomerAndStatus(Customer customer, Status.Customer status) {
        return customerOrderRepository.getTopByCustomerAndStatus(customer, status);
    }

    public CustomerOrderItem updateOrderItem(UserData user, Long productId, Integer count) {
        Customer customer = customerRepository.findByUsername(user.getUsername());
        if (customer == null) {
            return null;
        }
        CustomerOrder customerOrder = customerOrderRepository.findByCustomerAndStatus(customer, Status.Customer.DRAFT);
        if (customerOrder != null) {
            CustomerOrderItem customerOrderItem = updateCustomerOrderItem(customer, customerOrder, productId, count);
            if (customerOrderItem != null) {
                return customerOrderItem;
            }
        }
        if (count > 0) {
            SellPrice sellPrice = sellPriceRepository.findTopByProductIdOrderByCreatedDesc(productId);
            if (sellPrice == null) {
                return null;
            }
            StockReserve stockReserve = stockReserveRepository.findByProductId(sellPrice.getProductId());
            if (customerOrder == null) {
                customerOrder = createCustomerOrder(customer, stockReserve.getStock());
            }
            return createCustomerOrderItem(customer, customerOrder, stockReserve, sellPrice, count);
        }
        return null;
    }

    private CustomerOrderItem updateCustomerOrderItem(Customer customer, CustomerOrder customerOrder, Long productId, Integer count) {
        for (CustomerOrderItem coi : customerOrder.getCustomerOrderItems()) {
            if (coi.getSellPrice().getProductId().equals(productId)) {
                if (count > 0) {
                    int oldCount = coi.getCount();
                    StockReserve stockReserve = stockReserveRepository.findByProductId(productId);
                    if (stockReserve != null) {
                        if ((customerOrder.getReserved() != null ? (count - coi.getCount()) : count) <= stockReserve.getReserve()) {
//                            if (customer.getPhone() != null && customer.getPhoneVerified()) {
//                                tryReserve(customerOrder, stockReserve, coi.getCount(), count);
//                            }
                            coi.setCount(count);
                        } else {
//                            if (customer.getPhone() != null && customer.getPhoneVerified()) {
//                                tryReserve(customerOrder, stockReserve, coi.getCount(), stockReserve.getReserve());
//                            }
                            coi.setCount(stockReserve.getReserve());
                        }
                        BigDecimal oldSum = coi.getSum();
                        BigDecimal oldWeight = coi.getWeight();
                        BigDecimal oldVolume = coi.getVolume();
                        fillCustomerOrder(customerOrder, coi, oldSum, oldWeight, oldVolume, oldCount);
                        customerOrderItemRepository.save(coi);
                        customerOrderRepository.save(customerOrder);
                    } else {
                        onDeleteOrderItem(customerOrder, coi);
                        customerOrderRepository.save(customerOrder);
                        customerOrderItemRepository.delete(coi);
                    }
                } else {
                    onDeleteOrderItem(customerOrder, coi);
                    coi.setCount(0);
                    customerOrderRepository.save(customerOrder);
                    customerOrderItemRepository.delete(coi);
                }
                return coi;
            }
        }
        return null;
    }

    private void onDeleteOrderItem(CustomerOrder customerOrder, CustomerOrderItem coi) {
        customerOrder.setPcs(customerOrder.getPcs() - coi.getCount());
        customerOrder.setVolume(customerOrder.getVolume()
                .subtract(coi.getVolume())
                .setScale(2, RoundingMode.FLOOR)
        );
        customerOrder.setWeight(customerOrder.getWeight()
                .subtract(coi.getWeight())
                .setScale(2, RoundingMode.FLOOR)
        );
        customerOrder.setSum(customerOrder.getSum()
                .subtract(coi.getSum())
                .setScale(2, RoundingMode.FLOOR)
        );
    }

    private CustomerOrderItem createCustomerOrderItem(Customer customer, CustomerOrder customerOrder, StockReserve stockReserve,
                                                      SellPrice sellPrice, Integer count) {
        if (stockReserve == null) {
            return null;
        }
        CustomerOrderItem coi = new CustomerOrderItem();
        coi.setSellPrice(sellPrice);
//        if (customer.getPhone() != null && customer.getPhoneVerified()) {
//            tryReserve(customerOrder, stockReserve, 0, count);
//        }
        coi.setCount(count <= stockReserve.getReserve() ? count : stockReserve.getReserve());
        fillCustomerOrder(customerOrder, coi, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, 0);
        customerOrderRepository.save(customerOrder);
        coi.setCustomerOrder(customerOrder);
        return customerOrderItemRepository.save(coi);
    }

    private void fillCustomerOrder(CustomerOrder customerOrder, CustomerOrderItem customerOrderItem,
                                   BigDecimal oldSum, BigDecimal oldWeight, BigDecimal oldVolume, Integer oldCount) {
        customerOrderItem.setSum(customerOrderItem.getSellPrice().getPrice()
                .multiply(BigDecimal.valueOf(customerOrderItem.getCount()))
                .setScale(2, RoundingMode.FLOOR)
        );
        customerOrderItem.setWeight(customerOrderItem.getSellPrice().getProduct().getWeight()
                .multiply(BigDecimal.valueOf(customerOrderItem.getCount()))
                .setScale(2, RoundingMode.FLOOR)
        );
        customerOrderItem.setVolume(BigDecimal.valueOf(customerOrderItem.getCount())
                .multiply(customerOrderItem.getSellPrice().getProduct().getLength())
                .multiply(customerOrderItem.getSellPrice().getProduct().getWidth())
                .multiply(customerOrderItem.getSellPrice().getProduct().getHeight())
                .divide(BigDecimal.valueOf(1000000), MathContext.DECIMAL32)
                .setScale(2, RoundingMode.FLOOR)
        );
        customerOrder.setSum(customerOrder.getSum()
                .subtract(oldSum)
                .add(customerOrderItem.getSellPrice().getPrice()
                        .multiply(BigDecimal.valueOf(customerOrderItem.getCount()))
                )
                .setScale(2, RoundingMode.FLOOR)
        );
        customerOrder.setWeight(customerOrder.getWeight()
                .subtract(oldWeight)
                .add(customerOrderItem.getSellPrice().getProduct().getWeight()
                        .multiply(BigDecimal.valueOf(customerOrderItem.getCount()))
                )
                .setScale(2, RoundingMode.FLOOR)
        );
        customerOrder.setVolume(customerOrder.getVolume()
                .subtract(oldVolume)
                .add(customerOrderItem.getSellPrice().getProduct().getWidth()
                        .multiply(customerOrderItem.getSellPrice().getProduct().getHeight())
                        .multiply(customerOrderItem.getSellPrice().getProduct().getLength())
                        .multiply(BigDecimal.valueOf(customerOrderItem.getCount()))
                        .divide(BigDecimal.valueOf(1000000), MathContext.DECIMAL32)
                        .setScale(2, RoundingMode.FLOOR))
                .setScale(2, RoundingMode.FLOOR)
        );
        customerOrder.setPcs(customerOrder.getPcs() - oldCount + customerOrderItem.getCount());
    }

    private CustomerOrder createCustomerOrder(Customer customer, Stock stock) {
        CustomerOrder customerOrder = new CustomerOrder();
        customerOrder.setCustomerOrderItems(new HashSet<>());
        customerOrder.setStatus(Status.Customer.DRAFT);
        customerOrder.setCustomer(customer);
        customerOrder.setStock(stock);
        customerOrder.setSum(BigDecimal.ZERO);
        customerOrder.setWeight(BigDecimal.ZERO);
        customerOrder.setVolume(BigDecimal.ZERO);
        customerOrder.setPcs(0);
        return customerOrderRepository.save(customerOrder);
    }

    private void tryReserve(CustomerOrder customerOrder, StockReserve stockReserve, Integer oldCount, Integer newCount) {
        if (customerOrder.getReserved() != null) {
            if (stockReserve.getReserve() >= newCount - oldCount) {
                stockReserve.setReserve(stockReserve.getReserve() - (newCount - oldCount));
                customerOrder.setReserved(LocalDateTime.now(UTC));
                stockReserveRepository.save(stockReserve);
            }
        } else {
            stockReserve.setReserve(stockReserve.getReserve() - newCount);
            customerOrder.setReserved(LocalDateTime.now(UTC));
            stockReserveRepository.save(stockReserve);
        }
    }

    public Set<CustomerOrder> getOrders(Short page, Set<Status.Customer> statuses) {
        List<CustomerOrder> orders = customerOrderRepository.findAllByStatusInOrderByUpdatedDesc(
                statuses.size() == 0 ? Set.of(Status.Customer.values()) : statuses);
        for (CustomerOrder order : orders) {
            order.setPcs(order.getCustomerOrderItems().size());
        }
        return new HashSet<>(orders.subList(Math.min(orders.size(), page * 10),
                Math.min(orders.size(), (page + 1) * 10)));
    }

    public CustomerOrder getOrder(Long id) {
        return customerOrderRepository.findCustomerOrderById(id);
    }

    public CustomerOrder getOrder(UserData user) {
        Customer customer = customerRepository.findByUsername(user.getUsername());
        if (customer == null) {
            return null;
        }
        return customerOrderRepository.findByCustomerAndStatus(customer, Status.Customer.DRAFT);
    }
}
