package com.shop.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.shop.entity.Status;
import com.shop.entity.customer.*;
import com.shop.entity.delivery.DeliveryZone;
import com.shop.entity.stock.Stock;
import com.shop.entity.user.UserData;
import com.shop.repository.customer.*;
import com.shop.repository.product.description.CommonProductRepository;
import com.shop.repository.stock.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDateTime;
import java.util.Set;

import static com.shop.config.Constant.OBJECT_MAPPER;
import static java.time.temporal.ChronoUnit.SECONDS;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final CommonProductRepository commonProductRepository;
    private final CustomerFavoriteRepository customerFavoriteRepository;
    private final CustomerOrderRepository customerOrderRepository;
    private final CustomerAddressRepository customerAddressRepository;
    private final CustomerPhoneRepository customerPhoneRepository;
    private final StockRepository stockRepository;
    private final HttpRequest.Builder requestBuilder;
    private final HttpClient httpClient;
    private static final DeliveryZone emptyDeliveryZone = new DeliveryZone();
    private static final CustomerPhone emptyCustomerPhone = new CustomerPhone();

    @Autowired
    public CustomerService(CustomerRepository customerRepository, CommonProductRepository commonProductRepository,
                           CustomerFavoriteRepository customerFavoriteRepository,
                           CustomerOrderRepository customerOrderRepository, CustomerAddressRepository customerAddressRepository,
                           HttpRequest.Builder requestBuilder, HttpClient httpClient, CustomerPhoneRepository customerPhoneRepository,
                           StockRepository stockRepository) {
        this.customerRepository = customerRepository;
        this.commonProductRepository = commonProductRepository;
        this.customerFavoriteRepository = customerFavoriteRepository;
        this.customerOrderRepository = customerOrderRepository;
        this.customerAddressRepository = customerAddressRepository;
        this.requestBuilder = requestBuilder;
        this.httpClient = httpClient;
        this.customerPhoneRepository = customerPhoneRepository;
        this.stockRepository = stockRepository;
    }

    public Customer getCustomer(UserData user) {
        return customerRepository.findByUsername(user.getUsername());
    }

    public CustomerAddress getCustomerAddress(Customer customer, Long addressId, boolean withZoneTime) {
        if (customer == null) {
            return null;
        }
        if (withZoneTime) {
            return customerAddressRepository.getFirstByCustomerAndId(customer, addressId);
        } else {
            return customerAddressRepository.getByCustomerAndId(customer, addressId);
        }
    }

    public void addFavorite(Long commonProductId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof UserData) {
            commonProductRepository.findById(commonProductId).ifPresent(commonProduct -> {
                Customer customer = customerRepository
                        .findByUsername(((UserData) authentication.getPrincipal()).getUsername());
                customerFavoriteRepository.save(new CustomerFavorite(customer, commonProduct));
            });
        }
    }

    public Set<CustomerAddress> getAddresses() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof UserData) {
            Customer customer = customerRepository
                    .findByUsername(((UserData) authentication.getPrincipal()).getUsername());
            return customerAddressRepository.getAllByCustomerAndRetiredFalseOrderByUpdatedDesc(customer);
        }
        return null;
    }

    public CustomerAddress addAddress(CustomerAddress address) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof UserData) {
            Customer customer = customerRepository
                    .findByUsername(((UserData) authentication.getPrincipal()).getUsername());
            return addAddress(customer, address);
        }
        return null;
    }

    private CustomerAddress addAddress(Customer customer, CustomerAddress address) {
        address.setCustomerId(customer.getId());
        address.setRetired(false);
        DeliveryZone deliveryZone = getDeliveryZone(address.getLon(), address.getLat());
        if (deliveryZone != null) {
            address.setDeliveryZone(deliveryZone);
            return customerAddressRepository.save(address);
        }
        return null;
    }

    public CustomerAddress updateAddress(CustomerAddress address) {
        if (address.getId() == null) {
            return address;
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof UserData) {
            Customer customer = customerRepository
                    .findByUsername(((UserData) authentication.getPrincipal()).getUsername());
            CustomerAddress oldAddress = customerAddressRepository.findByCustomerAndId(customer, address.getId());
            if (address.equals(oldAddress)) {
                return address;
            }
            Set<CustomerOrder> linkedOrders = customerOrderRepository
                    .findAllByCustomerAndAddressAndStatusNot(customer, oldAddress, Status.Customer.DRAFT);
            if (linkedOrders.size() > 0) {
                oldAddress.setRetired(true);
                customerAddressRepository.save(oldAddress);
                address.setId(null);
            }
            return addAddress(customer, address);
        }
        return address;
    }

    public DeliveryZone getDeliveryZone(double lon, double lat) {
        Set<Stock> stocks = stockRepository.findAll();
        for (Stock stock : stocks) {
            for (DeliveryZone deliveryZone : stock.getDeliveryZones()) {
                try {
                    HttpRequest request = requestBuilder
                            .uri(new URI(String.format(
                                    "http://localhost:5000/route/v1/car/%f,%f;%f,%f?overview=false",
                                    deliveryZone.getLon(), deliveryZone.getLat(), lon, lat
                                    ))
                            ).GET().build();
                    HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
                    JsonNode jsonNode = OBJECT_MAPPER.readTree(response.body());
                    if (jsonNode.get("routes").size() > 0 && jsonNode.get("routes").get(0).get("duration").asDouble() < deliveryZone.getMaxDuration()
                            && jsonNode.get("routes").get(0).get("duration").asDouble() >= deliveryZone.getMinDuration()) {
                        return deliveryZone;
                    }
                } catch (URISyntaxException | IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return emptyDeliveryZone;
    }

    public Boolean getCustomerFavorite(Long commonProductId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof UserData) {
            CustomerFavorite customerFavorite = customerFavoriteRepository
                    .getFavoriteProduct(authentication.getName(), commonProductId);
            return customerFavorite != null;
        }
        return false;
    }

    public void deleteFavorite(Long commonProductId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof UserData) {
            commonProductRepository.findById(commonProductId).ifPresent(commonProduct -> {
                Customer customer = customerRepository
                        .findByUsername(((UserData) authentication.getPrincipal()).getUsername());
                customerFavoriteRepository.delete(new CustomerFavorite(customer, commonProduct));
            });
        }
    }

    public Customer getDetails(UserData user) {
        Customer customer = customerRepository.getByUsername(user.getUsername());
        CustomerOrder customerOrder = customerOrderRepository.findByCustomerAndStatus(customer, Status.Customer.DRAFT);
        if (customerOrder != null) {
            customer.setOrders(Set.of(customerOrder));
        }
        return customer;
    }

    public CustomerPhone verifyPhone(UserData user, String phone, String code) {
        Customer customer = customerRepository.getByUsername(user.getUsername());
        CustomerPhone customerPhone = customerPhoneRepository.findByCustomerAndPhoneAndCode(customer, phone, code);
        if (customerPhone != null && SECONDS.between(customerPhone.getCreated(), LocalDateTime.now()) < 90) {
            customerPhone.setVerified(true);
            return customerPhoneRepository.save(customerPhone);
        }
        return customerPhone != null ? customerPhone : emptyCustomerPhone;
    }

    public CustomerPhone sendVerifyingCode(UserData user, String phone) {
        Customer customer = customerRepository.findByUsername(user.getUsername());
        CustomerPhone customerPhone = customerPhoneRepository.findByCustomerAndPhone(customer, phone);
        if (customerPhone != null) {
            if (customerPhone.getVerified()) {
                return customerPhone;
            } else {
                customerPhoneRepository.delete(customerPhone);
            }
        }
        customerPhone = new CustomerPhone();
        customerPhone.setCustomer(customer);
        customerPhone.setPhone(phone);
        customerPhone.setVerified(false);
        customerPhone.setCode(sendCode(phone));
        CustomerPhone cp = customerPhoneRepository.save(customerPhone);
        cp.setCustomer(null);
        return cp;
    }

    private String sendCode(String phone) {
        return phone;
    }

    public Customer updateCustomer(UserData user, Customer newCustomer) {
        Customer customer = customerRepository.findByUsername(user.getUsername());
        boolean isChanged = false;
        if (newCustomer.getName() != null && !newCustomer.getName().equals(customer.getName())) {
            customer.setName(newCustomer.getName());
            isChanged = true;
        }
        return isChanged ? customerRepository.save(customer) : customer;
    }

    public Set<CustomerPhone> getPhones(UserData user, boolean verified) {
        Customer customer = customerRepository.findByUsername(user.getUsername());
        return customerPhoneRepository.findAllByCustomerAndVerifiedOrderByCreatedDesc(customer, verified);
    }
}
