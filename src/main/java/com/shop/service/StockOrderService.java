package com.shop.service;

import com.shop.entity.Status;
import com.shop.entity.product.price.BuyPrice;
import com.shop.entity.product.price.SellPrice;
import com.shop.entity.stock.StockOrder;
import com.shop.entity.stock.StockOrderItem;
import com.shop.entity.stock.StockReserve;
import com.shop.repository.product.price.BuyPriceRepository;
import com.shop.repository.product.price.SellPriceRepository;
import com.shop.repository.stock.StockOrderItemRepository;
import com.shop.repository.stock.StockOrderRepository;
import com.shop.repository.stock.StockReserveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.shop.entity.Status.Stock.DRAFT;

@Service
public class StockOrderService {

    private final StockOrderRepository stockOrderRepository;
    private final StockOrderItemRepository stockOrderItemRepository;
    private final BuyPriceRepository buyPriceRepository;
    private final SellPriceRepository sellPriceRepository;
    private final StockReserveRepository stockReserveRepository;

    @Autowired
    public StockOrderService(StockOrderRepository stockOrderRepository,
                             StockOrderItemRepository stockOrderItemRepository,
                             BuyPriceRepository buyPriceRepository,
                             StockReserveRepository stockReserveRepository,
                             SellPriceRepository sellPriceRepository) {
        this.stockOrderRepository = stockOrderRepository;
        this.stockOrderItemRepository = stockOrderItemRepository;
        this.buyPriceRepository = buyPriceRepository;
        this.stockReserveRepository = stockReserveRepository;
        this.sellPriceRepository = sellPriceRepository;
    }

    public StockOrder updateOrder(StockOrder stockOrder) {
        StockOrder oldStockOrder = stockOrderRepository.findStockOrderById(stockOrder.getId());
        if (oldStockOrder.getStatus().equals(DRAFT)) {
            stockOrder.setStock(oldStockOrder.getStock());
            for (StockOrderItem orderItem : stockOrder.getStockOrderItems()) {
                if (orderItem.getId() == null) {
                    if (orderItem.getCount() > 0 && orderItem.getBuyPrice().getPrice().doubleValue() > 0) {
                        orderItem.getBuyPrice().setPricePerUnit(
                                orderItem.getBuyPrice().getPrice().divide(orderItem.getBuyPrice().getProduct().getPackVolume(), MathContext.DECIMAL32));
                    }
                }
            }
            Set<StockOrderItem> itemsForDelete = new HashSet<>();
            oldStockOrder.getStockOrderItems().forEach(oi -> {
                boolean forDelete = true;
                for (StockOrderItem soi : stockOrder.getStockOrderItems()) {
                    if (oi.getId().equals(soi.getId())) {
                        forDelete = false;
                        break;
                    }
                }
                if (forDelete) {
                    itemsForDelete.add(oi);
                }
            });
            stockOrderItemRepository.deleteAll(itemsForDelete);
            BigDecimal sum = BigDecimal.ZERO;
            for (StockOrderItem oi : stockOrder.getStockOrderItems()) {
                sum = sum.add(oi.getBuyPrice().getPrice().multiply(BigDecimal.valueOf(oi.getCount())));
                for (StockOrderItem soi : oldStockOrder.getStockOrderItems()) {
                    if (oi.getBuyPrice().getId() != null && oi.getBuyPrice().getId().equals(soi.getBuyPrice().getId())) {
                        if (!oi.getBuyPrice().getPrice().equals(soi.getBuyPrice().getPrice())) {
                            oi.getBuyPrice().setId(null);
                            buyPriceRepository.save(oi.getBuyPrice());
                        }
                        break;
                    }
                }
                if (oi.getId() == null && oi.getBuyPrice() != null) {
                    if (oi.getBuyPrice().getId() != null) {
                        BuyPrice buyPrice = buyPriceRepository.findTopById(oi.getBuyPrice().getId());
                        if (!buyPrice.getPrice().equals(oi.getBuyPrice().getPrice())) {
                            oi.getBuyPrice().setId(null);
                        }
                    }
                    buyPriceRepository.save(oi.getBuyPrice());
                }
            }
            stockOrder.setSum(sum);
            stockOrderItemRepository.saveAll(stockOrder.getStockOrderItems());
            stockOrderRepository.save(stockOrder);
        }
        return stockOrder;
    }

    public Set<StockOrder> getStockOrders(Short page, Set<Status.Stock> statuses) {
        List<StockOrder> stockOrders = stockOrderRepository.findAllByStatusInOrderByUpdatedDesc(
                statuses.size() == 0 ? Set.of(Status.Stock.values()) : statuses);
        for (StockOrder stockOrder : stockOrders) {
            int pcs = 0;
            for (StockOrderItem stockOrderItem : stockOrder.getStockOrderItems()) {
                pcs += stockOrderItem.getCount();
            }
            stockOrder.setPcs(pcs);
        }
        return new HashSet<>(stockOrders.subList(Math.min(stockOrders.size(), page * 10),
                Math.min(stockOrders.size(), (page + 1) * 10)));
    }

    public StockOrder createOrder(StockOrder order) {
        StockOrder stockOrder = stockOrderRepository.findByStatusAndStockId(DRAFT, order.getStockId());
        if (stockOrder != null) {
            return stockOrder;
        }
        order.setStatus(DRAFT);
        order.setSum(BigDecimal.ZERO);
        order.setPcs(0);
        order.setVolume(BigDecimal.ZERO);
        order.setWeight(BigDecimal.ZERO);
        return stockOrderRepository.save(order);
    }

    public StockOrder getOrder(Long stockOrderId) {
        return stockOrderRepository.findStockOrderById(stockOrderId);
    }

    @Transactional
    public StockOrder changeStatus(Long orderId, Status.Stock status) {
        StockOrder stockOrder = stockOrderRepository.findStockOrderById(orderId);
        switch (status) {
            case DRAFT:
                if (stockOrder.getStatus().equals(Status.Stock.CANCEL)) {
                    stockOrder.setStatus(DRAFT);
                }
                break;
            case FINALIZE:
                if (stockOrder.getStatus().equals(DRAFT)) {
                    stockOrder.setStatus(Status.Stock.FINALIZE);
                }
                break;
            case ORDER:
                if (stockOrder.getStatus().equals(Status.Stock.FINALIZE)) {
                    stockOrder.setStatus(Status.Stock.ORDER);
                }
                break;
            case DELIVER:
                if (stockOrder.getStatus().equals(Status.Stock.ORDER)) {
                    stockOrder.setStatus(Status.Stock.DELIVER);
                }
                break;
            case COMPLETE:
                if (stockOrder.getStatus().equals(Status.Stock.DELIVER)) {
                    stockOrder.setStatus(Status.Stock.COMPLETE);
                    Set<StockReserve> stockReserves = new HashSet<>();
                    Set<SellPrice> sellPrices = new HashSet<>();
                    for (StockOrderItem stockOrderItem : stockOrder.getStockOrderItems()) {
                        StockReserve stockReserve = stockReserveRepository.findByProductId(stockOrderItem.getBuyPrice().getProduct().getId());
                        if (stockReserve == null) {
                            stockReserve = new StockReserve();
                            stockReserve.setProductId(stockOrderItem.getBuyPrice().getProduct().getId());
                            stockReserve.setStockId(stockOrder.getStock().getId());
                            stockReserve.setReserve(stockOrderItem.getCount());
                        } else {
                            stockReserve.setReserve(stockReserve.getReserve() + stockOrderItem.getCount());
                        }
                        stockReserves.add(stockReserve);
                        SellPrice sellPrice = new SellPrice(stockOrderItem.getBuyPrice());
                        sellPrices.add(sellPrice);
                    }
                    stockReserveRepository.saveAll(stockReserves);
                    sellPriceRepository.saveAll(sellPrices);
                }
                break;
            case CANCEL:
                if (!stockOrder.getStatus().equals(Status.Stock.COMPLETE)) {
                    stockOrder.setStatus(Status.Stock.CANCEL);
                }
                break;
        }
        return stockOrderRepository.save(stockOrder);
    }
}
