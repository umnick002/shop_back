package com.shop.service;

import com.shop.entity.MimeType;
import com.shop.entity.customer.CommonProductComment;
import com.shop.entity.customer.Customer;
import com.shop.entity.product.description.*;
import com.shop.entity.product.price.BuyPrice;
import com.shop.entity.product.price.SellPrice;
import com.shop.entity.stock.StockReserve;
import com.shop.entity.user.UserData;
import com.shop.repository.customer.CustomerRepository;
import com.shop.repository.product.description.*;
import com.shop.repository.product.price.BuyPriceRepository;
import com.shop.repository.product.price.SellPriceRepository;
import com.shop.repository.stock.StockReserveRepository;
import com.shop.wrapper.CommonProductsWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.File;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.shop.config.Constant.PRODUCTS_PER_PAGE;
import static java.time.ZoneOffset.UTC;

@Service
public class CommonProductService {

    private static Logger LOGGER = LoggerFactory.getLogger(CommonProductService.class);

    private final CommonProductRepository commonProductRepository;
    private final CommonProductCommentRepository commonProductCommentRepository;
    private final CustomerRepository customerRepository;
    private final ContentRepository contentRepository;
    private final PropertyRepository propertyRepository;
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final BuyPriceRepository buyPriceRepository;
    private final SellPriceRepository sellPriceRepository;
    private final StockReserveRepository stockReserveRepository;
    @PersistenceContext
    private EntityManager em;

    private final MessageDigest md;

    private static final String CONTENT_PATH = "/home/umnick02/Documents/";
    private static final String RELATIVE_CONTENT_PATH = "images/";

    @Autowired
    public CommonProductService(CommonProductRepository commonProductRepository,
                                CommonProductCommentRepository commonProductCommentRepository,
                                CustomerRepository customerRepository, ContentRepository contentRepository,
                                PropertyRepository propertyRepository, ProductRepository productRepository, CategoryRepository categoryRepository, BuyPriceRepository buyPriceRepository, SellPriceRepository sellPriceRepository, StockReserveRepository stockReserveRepository)
    throws NoSuchAlgorithmException {
        this.commonProductRepository = commonProductRepository;
        this.commonProductCommentRepository = commonProductCommentRepository;
        this.customerRepository = customerRepository;
        this.contentRepository = contentRepository;
        md = MessageDigest.getInstance("MD5");
        this.propertyRepository = propertyRepository;
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.buyPriceRepository = buyPriceRepository;
        this.sellPriceRepository = sellPriceRepository;
        this.stockReserveRepository = stockReserveRepository;
    }

    public CommonProduct getCommonProduct(Long id) {
        CommonProduct commonProduct = commonProductRepository.findCommonProductById(id);
        if (commonProduct != null) {
            Set<StockReserve> stockReserves = stockReserveRepository.findReservedProducts(
                    commonProduct.getProducts().stream().map(Product::getId).collect(Collectors.toSet()),
                    LocalDateTime.now(UTC).minusDays(7));
            commonProduct.getProducts().forEach(product -> {
                fillReserved(stockReserves, product);
            });
            removeAbsent(commonProduct);
            LOGGER.info("Get product with id [{}]", commonProduct.getId());
            return commonProduct;
        }
        return null;
    }

    public Set<CommonProduct> getCommonProductsByCategory(Long mainId, Long secondaryId) {
        Set<CommonProduct> commonProducts = null;
        if (secondaryId != null) {
            commonProducts = commonProductRepository.findAllBySubCategory_Id(secondaryId);
        } else if (mainId != null) {
            commonProducts = commonProductRepository.findAllByCategory_Id(mainId);
        }
        if (commonProducts != null && commonProducts.size() > 0) {
            Set<Long> productIds = new HashSet<>();
            commonProducts.forEach(commonProduct -> {
                commonProduct.getProducts().forEach(product -> productIds.add(product.getId()));
                if (commonProduct.getContents().size() > 0) {
                    Content content = commonProduct.getContents().iterator().next();
                    commonProduct.setContents(Set.of(content));
                }
            });
            Set<StockReserve> stockReserves = stockReserveRepository.findReservedProducts(productIds, LocalDateTime.now(UTC).minusDays(7));
            commonProducts.forEach(commonProduct -> commonProduct.getProducts().forEach(product -> {
                fillReserved(stockReserves, product);
            }));
            for (CommonProduct commonProduct : commonProducts) {
                removeAbsent(commonProduct);
            }
        }
        return commonProducts;
    }

    private void fillReserved(Set<StockReserve> stockReserves, Product product) {
        for (StockReserve stockReserve : stockReserves) {
            if (stockReserve.getProductId().equals(product.getId())) {
                product.setReserved(stockReserve.getReserve() > 0);
                return;
            }
        }
        product.setReserved(false);
    }

    private void removeAbsent(CommonProduct commonProduct) {
        boolean reserved = false;
        for (Product product : commonProduct.getProducts()) {
            if (product.getReserved()) {
                reserved = true;
                break;
            }
        }
        if (reserved) {
            commonProduct.getProducts().removeIf(product -> !product.getReserved());
        }
    }

    public Set<CommonProduct> getCommonProductsByCategory(Long mainId) {
        return getCommonProductsByCategory(mainId, null);
    }


    public CommonProductComment addComment(Long commonProductId, String comment, Short rating) {
        if (rating > 0) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication.getPrincipal() instanceof UserData) {
                Customer customer = customerRepository
                        .findByUsername(((UserData) authentication.getPrincipal()).getUsername());
                CommonProductComment commonProductComment = commonProductCommentRepository
                        .findByCustomerIdAndCommonProductId(customer.getId(), commonProductId);
                if (commonProductComment == null) {
                    commonProductComment = new CommonProductComment();
                    commonProductComment.setCustomerId(customer.getId());
                    commonProductComment.setCommonProductId(commonProductId);
                }
                commonProductComment.setComment(comment);
                commonProductComment.setRating(rating);
                commonProductComment.setCreated(LocalDateTime.now(UTC));
                CommonProductComment productComment = commonProductCommentRepository.save(commonProductComment);
                List<Short> comments = commonProductCommentRepository.getRatingByCommonProductId(commonProductId);
                short s = 0;
                for (Short c : comments) {
                    s += c;
                }
                commonProductRepository.updateRating(commonProductId, (double) s/comments.size());
                productComment.setCustomerId(null);
                productComment.setCustomer(null);
                productComment.setCommonProductId(null);
                productComment.setCommonProduct(null);
                return productComment;
            }
        }
        return null;
    }

    public Set<CommonProductComment> getComments(Long commonProductId) {
        Set<CommonProductComment> comments = commonProductCommentRepository
                .findAllByCommonProductId(commonProductId);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CommonProductComment customerComment = null;
        if (authentication.getPrincipal() instanceof UserData) {
            Customer customer = customerRepository
                    .findByUsername(((UserData) authentication.getPrincipal()).getUsername());
            customerComment = commonProductCommentRepository
                    .findByCustomerIdAndCommonProductId(customer.getId(), commonProductId);
        }
        for (CommonProductComment comment : comments) {
            if (comment.equals(customerComment)) {
                comment.setAuthor(true);
            }
//            comment.setCustomerId(null);
//            comment.getCustomer().setCommonProductComments(null);
//            comment.getCustomer().setOrders(null);
//            comment.getCustomer().setFavorites(null);
//            comment.getCustomer().setCreated(null);
//            comment.getCustomer().setUpdated(null);
//            comment.setCommonProductId(null);
//            comment.setCommonProduct(null);
        }
        return comments;
    }

    public Content addContent(Long id, String contentType, byte[] data) throws Exception {
        MimeType mimeType = null;
        for (MimeType mt : MimeType.values()) {
            if (mt.toString().equals(contentType)) {
                mimeType = mt;
                break;
            }
        }
        if (mimeType == null) {
            throw new NullPointerException("Unrecognized Content-Type");
        }
        String suffix = mimeType.getSuffix();
        String fileName = String.format("%s%s%s%s", CONTENT_PATH, RELATIVE_CONTENT_PATH, UUID.randomUUID().toString(), suffix);
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(data);
        }
        byte[] hash = md.digest(data);
        Content content = contentRepository.findByHash(hash);
        if (id == null && content != null && content.getCommonProductId() != null) {
            throw new IllegalArgumentException("Photo already exists on common product with id=" + content.getCommonProductId());
        }
        if (content == null) {
            content = new Content();
            content.setHash(hash);
            content.setFile(fileName.replace(CONTENT_PATH, "").replace(RELATIVE_CONTENT_PATH, ""));
            if (id != null) {
                content.setCommonProductId(id);
            }
        }
        content.setMimeType(mimeType);
        Short maxPriority = contentRepository.findMaxPriorityByCommonProductId(id);
        content.setPriority((short)(maxPriority + 1));
        return contentRepository.save(content);
    }

    public CommonProduct saveCommonProduct(CommonProduct commonProduct) {
        validateCommonProduct(commonProduct);
        if (commonProduct.getId() != null) {
            return updateOldProduct(commonProduct);
        }
        return saveNewProduct(commonProduct);
    }

    private CommonProduct updateOldProduct(CommonProduct commonProduct) {
        CommonProduct oldCommonProduct = commonProductRepository.getCommonProductById(commonProduct.getId());
        oldCommonProduct.setContents(updateContents(oldCommonProduct.getContents(), commonProduct.getContents(), oldCommonProduct.getId()));
        oldCommonProduct.setProperties(updateProperties(oldCommonProduct.getProperties(), commonProduct.getProperties(), oldCommonProduct.getId()));
        oldCommonProduct.setProducts(updateProducts(oldCommonProduct.getProducts(), commonProduct.getProducts(), oldCommonProduct.getId()));
        oldCommonProduct.setName(commonProduct.getName());
        oldCommonProduct.setDescription(commonProduct.getDescription());
        oldCommonProduct.setSubCategory(commonProduct.getSubCategory());
        oldCommonProduct.setByWeight(commonProduct.getByWeight());
        return commonProductRepository.save(oldCommonProduct);
    }

    private CommonProduct saveNewProduct(CommonProduct commonProduct) {
        Set<Product> products = new HashSet<>(commonProduct.getProducts());
        Set<Property> properties = new HashSet<>(commonProduct.getProperties());
        Set<Content> contents = new HashSet<>(commonProduct.getContents());
        commonProduct.setProducts(null);
        commonProduct.setProperties(null);
        commonProduct.setContents(null);
        commonProductRepository.save(commonProduct);
        products.forEach(product -> product.setCommonProductId(commonProduct.getId()));
        properties.forEach(property -> property.setCommonProductId(commonProduct.getId()));
        long minContentId = Collections.min(contents.stream().map(Content::getId).collect(Collectors.toList()));
        contents.forEach(content -> {
            content.setCommonProductId(commonProduct.getId());
            content.setPriority((short) (content.getId() - minContentId + 1));
        });
        commonProduct.setContents(contents);
        commonProduct.setProperties(properties);
        commonProduct.setProducts(products);
        return commonProductRepository.save(commonProduct);
    }

    private Set<Content> updateContents(Set<Content> oldContents, Set<Content> newContents, Long commonProductId) {
        newContents.forEach(content -> content.setCommonProductId(commonProductId));
        Set<Content> contentsForDelete = new HashSet<>();
        oldContents.forEach(content -> {
            boolean forDelete = true;
            for (Content c : newContents) {
                if (c.equals(content)) {
                    forDelete = false;
                    break;
                }
            }
            if (forDelete) {
                contentsForDelete.add(content);
            }
        });
        contentRepository.deleteAll(contentsForDelete);
        contentsForDelete.forEach(content -> {
            File file = new File(CONTENT_PATH + content.getFile());
            if (file.exists()) {
                file.delete();
            }
        });
        return newContents;
    }

    private Set<Property> updateProperties(Set<Property> oldProperties, Set<Property> newProperties, Long commonProductId) {
        newProperties.forEach(property -> property.setCommonProductId(commonProductId));
        Set<Property> propertiesForDelete = new HashSet<>();
        oldProperties.forEach(property -> {
            boolean forDelete = true;
            for (Property p : newProperties) {
                if (p.equals(property)) {
                    p.setId(property.getId());
                    forDelete = false;
                    break;
                }
            }
            if (forDelete) {
                propertiesForDelete.add(property);
            }
        });
        propertyRepository.deleteAll(propertiesForDelete);
        return newProperties;
    }

    private Set<Product> updateProducts(Set<Product> oldProducts, Set<Product> newProducts, Long commonProductId) {
        newProducts.forEach(product -> product.setCommonProductId(commonProductId));
        Set<Product> productsForDelete = new HashSet<>();
        oldProducts.forEach(product -> {
            boolean forDelete = true;
            for (Product p : newProducts) {
                if (p.equals(product)) {
                    p.setId(product.getId());
                    forDelete = false;
                    break;
                }
            }
            if (forDelete) {
                productsForDelete.add(product);
            }
        });
        productRepository.deleteAll(productsForDelete);
        return newProducts;
    }

    private void validateCommonProduct(CommonProduct commonProduct) {
        Assert.hasText(commonProduct.getName(), "Required field 'name'");
        Assert.hasText(commonProduct.getDescription(), "Required field 'description'");
        Assert.notEmpty(commonProduct.getContents(), "Required entries for 'contents'");
        Assert.notEmpty(commonProduct.getProperties(), "Required entries for 'properties'");
        Assert.notEmpty(commonProduct.getProducts(), "Required entries for 'products'");
        Assert.notNull(commonProduct.getSubCategory(), "Required field for 'subCategory'");
    }

    public CommonProductsWrapper getCommonProductsByFilter(Long id, String name, Long categoryId,
                                                           Long subCategoryId, Short page) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<CommonProduct> query = builder.createQuery(CommonProduct.class);
        Root<CommonProduct> root = query.from(CommonProduct.class);
        root.fetch("contents", JoinType.LEFT);
        root.fetch("subCategory", JoinType.LEFT).fetch("category", JoinType.LEFT);
        root.fetch("products", JoinType.LEFT);
        if (id != null) {
            Predicate filterId = builder.equal(root.get("id"), id);
            query.where(filterId);
        }
        if (!name.equals("")) {
            Predicate filterName = builder.like(builder.lower(root.get("name")), "%" + name.toLowerCase() + "%");
            query.where(filterName);
        }
        if (!categoryId.equals(-1L)) {
            Set<Long> ids = new HashSet<>();
            if (subCategoryId.equals(-1L)) {
                Category category = categoryRepository.findSubCategoriesById(categoryId);
                category.getSubCategories().forEach(subCategory -> ids.add(subCategory.getId()));
            } else {
                ids.add(subCategoryId);
            }
            Predicate filterCategory = root.get("subCategoryId").in(ids);
            query.where(filterCategory);
        }
        query.distinct(true);
        query.orderBy(builder.desc(root.get("updated")));
        List<CommonProduct> commonProducts = em.createQuery(query.select(root)).getResultList();
        Set<Long> productIds = new HashSet<>();
        commonProducts.forEach(commonProduct -> commonProduct.getProducts().forEach(product -> productIds.add(product.getId())));
        Set<SellPrice> sellPrices = sellPriceRepository.findByProductIdInOrderByCreatedDesc(productIds);
        Set<BuyPrice> buyPrices = buyPriceRepository.findByProductIdInOrderByCreatedDesc(productIds);
        for (CommonProduct commonProduct : commonProducts) {
            for (Product product : commonProduct.getProducts()) {
                for (SellPrice sellPrice : sellPrices) {
                    if (sellPrice.getProductId().equals(product.getId())) {
                        product.setSellPrices(Set.of(sellPrice));
                        break;
                    }
                }
                for (BuyPrice buyPrice : buyPrices) {
                    if (buyPrice.getProductId().equals(product.getId())) {
                        product.setBuyPrices(Set.of(buyPrice));
                        break;
                    }
                }
                product.setCommonProduct(commonProduct);
            }
        }
        CommonProductsWrapper commonProductsWrapper = new CommonProductsWrapper();
        Iterator<CommonProduct> iterator = commonProducts.iterator();
        int i = 0;
        List<CommonProduct> commonProductsPage = new ArrayList<>();
        while (iterator.hasNext()) {
            if (i >= Math.min(commonProducts.size(), (page - 1) * PRODUCTS_PER_PAGE) && i < Math.min(commonProducts.size(), page * PRODUCTS_PER_PAGE)) {
                i++;
                commonProductsPage.add(iterator.next());
            } else if (i >= Math.min(commonProducts.size(), page * PRODUCTS_PER_PAGE)) {
                break;
            } else {
                i++;
                iterator.next();
            }
        }
        commonProductsWrapper.setCommonProducts(commonProductsPage);
        commonProductsWrapper.setPages((int) Math.ceil(commonProducts.size() / (double) PRODUCTS_PER_PAGE));
        return commonProductsWrapper;
    }
}
