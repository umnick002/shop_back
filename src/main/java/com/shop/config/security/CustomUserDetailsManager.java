package com.shop.config.security;

import com.shop.entity.customer.Customer;
import com.shop.entity.user.Group;
import com.shop.entity.user.Authority;
import com.shop.entity.user.UserData;
import com.shop.repository.customer.CustomerRepository;
import com.shop.repository.user.GroupRepository;
import com.shop.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.GroupManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class CustomUserDetailsManager implements UserDetailsManager, GroupManager {

    private final UserRepository userRepository;
    private final GroupRepository groupRepository;
    private final CustomerRepository customerRepository;

    private static final PasswordEncoder PASSWORD_ENCODER = PasswordEncoderFactories.createDelegatingPasswordEncoder();

    @Autowired
    public CustomUserDetailsManager(UserRepository userRepository, GroupRepository groupRepository, CustomerRepository customerRepository) {
        this.userRepository = userRepository;
        this.groupRepository = groupRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserData userData = userRepository.findByUsername(username);
        if (userData == null) {
            throw new UsernameNotFoundException("Username [" + username + "] not found!");
        }
        return userData;
    }

    private UserData generateUserData() {
        UserData userData = new UserData();
        long username;
        do {
            username = ThreadLocalRandom.current().nextLong(1000000, 10000000);
        } while (userRepository.existsByUsername(Long.toString(username)));
        userData.setUsername(Long.toString(username));
        userData.setPassword(Long.toString(
                ThreadLocalRandom.current().nextLong(10_000_000_000L, 100_000_000_000L)));
        return userData;
    }

    public UserData handleCreateCustomer(Customer customer) {
        validateCustomer(customer);
        UserData user = createUserWithGroup("CUSTOMER");
        customer.setUsername(user.getUsername());
        customerRepository.save(customer);
        return user;
    }

    private void validateCustomer(Customer customer) {
        Assert.hasLength(customer.getName(), "name required");
//        if (customer.getPhone() != null && customer.getPhone().length() == 0) {
//            customer.setPhone(null);
//        }
        if (customer.getEmail() != null && customer.getEmail().length() == 0) {
            customer.setEmail(null);
        }
    }

    private UserData createUserWithGroup(String groupName) {
        UserData user = generateUserData();
        UserData decodedUser = new UserData(user.getUsername(), user.getPassword());
        Group group = groupRepository.findByName(groupName);
        if (group == null) {
            createGroup(groupName, new Authority("ROLE_" + groupName));
            group = groupRepository.findByName(groupName);
        }
        Set<Group> groups = new HashSet<>();
        groups.add(group);
        user.setGroups(groups);
        createUser(user);
        return decodedUser;
    }

    @Override
    @Transactional
    public void createUser(UserDetails userDetails) {
        ((UserData) userDetails).setPassword(PASSWORD_ENCODER.encode(userDetails.getPassword()));
        userRepository.save((UserData) userDetails);
    }

    @Override
    public void updateUser(final org.springframework.security.core.userdetails.UserDetails userDetails) {
        createUser(userDetails);
    }

    @Override
    public void deleteUser(String username) {
        userRepository.deleteByUsername(username);
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
        if (currentUser == null) {
            throw new AccessDeniedException(
                    "Can't change password as no Authentication object found in context for current userData.");
        }
        String username = currentUser.getName();
        UserData userData = userRepository.findByUsername(username);
        userData.setPassword(newPassword);
        userRepository.save(userData);
        SecurityContextHolder.getContext().setAuthentication(createNewAuthentication(currentUser, userData, newPassword));
    }

    @Override
    public boolean userExists(String username) {
        return userRepository.findByUsername(username) != null;
    }

    private Authentication createNewAuthentication(Authentication currentAuth, org.springframework.security.core.userdetails.UserDetails userDetails, String newPassword) {
        UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(
                userDetails, newPassword, userDetails.getAuthorities());
        newAuthentication.setDetails(currentAuth.getDetails());
        return newAuthentication;
    }

    @Override
    public List<String> findAllGroups() {
        Iterator<Group> groupIterator = groupRepository.findAll().iterator();
        List<String> groups = new ArrayList<>();
        while (groupIterator.hasNext()) {
            groups.add(groupIterator.next().getName());
        }
        return groups;
    }

    @Override
    public List<String> findUsersInGroup(String groupName) {
        Group group = groupRepository.findByName(groupName);
        Set<UserData> users = group.getUsers();
        List<String> userNames = new ArrayList<>();
        for (UserData user : users) {
            userNames.add(user.getUsername());
        }
        return userNames;
    }

    @Transactional
    public void createGroup(String groupName, GrantedAuthority authority) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(authority);
        createGroup(groupName, authorities);
    }

    @Override
    @Transactional
    public void createGroup(String groupName, List<GrantedAuthority> authorities) {
        Group group = groupRepository.findByName(groupName);
        if (group == null) {
            group = new Group(groupName);
            Set<Authority> groupAuthorities = new HashSet<>();
            for (GrantedAuthority grantedAuthority : authorities) {
                groupAuthorities.add((Authority) grantedAuthority);
                ((Authority) grantedAuthority).setGroup(group);
            }
            group.setAuthorities(groupAuthorities);
            groupRepository.save(group);
        }
    }

    @Override
    public void deleteGroup(String groupName) {
        groupRepository.deleteByName(groupName);
    }

    @Override
    public void renameGroup(String oldName, String newName) {
        Group group = groupRepository.findByName(oldName);
        group.setName(newName);
        groupRepository.save(group);
    }

    @Override
    @Transactional
    public void addUserToGroup(String username, String group) {
        UserData userData = userRepository.findByUsername(username);
        Group userGroup = groupRepository.findByName(group);
        userData.addGroup(userGroup);
        userRepository.save(userData);
    }

    @Override
    public void removeUserFromGroup(String username, String groupName) {
        UserData userData = userRepository.findByUsername(username);
        for (Group group : userData.getGroups()) {
            if (group.getName().endsWith(groupName)) {
                userData.getGroups().remove(group);
            }
        }
    }

    @Override
    public List<GrantedAuthority> findGroupAuthorities(String groupName) {
        Set<Authority> groupAuthorities = groupRepository.findByName(groupName).getAuthorities();
        return new ArrayList<>(groupAuthorities);
    }

    @Override
    public void addGroupAuthority(String groupName, GrantedAuthority authority) {
        Group group = groupRepository.findByName(groupName);
        group.getAuthorities().add((Authority) authority);
        groupRepository.save(group);
    }

    @Override
    public void removeGroupAuthority(String groupName, GrantedAuthority authority) {
        Group group = groupRepository.findByName(groupName);
        for (Authority groupAuthority : group.getAuthorities()) {
            if (groupAuthority.getAuthority().endsWith(authority.getAuthority())) {
                group.getAuthorities().remove(groupAuthority);
            }
        }
        groupRepository.save(group);
    }
}
