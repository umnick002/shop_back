package com.shop.config;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Constant {
    public static final double DELIVERY_LOAD = 0.85;
    public static final int DELIVERY_WAIT = 3*60;
    public static final int DELIVERY_TO_STORE = 10*60;
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    public static final int PRODUCTS_PER_PAGE = 6;

}
