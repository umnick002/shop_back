package com.shop.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;

@Configuration
@Profile("dev")
@Order(1)
public class MainConfig {

    @Bean
    public HttpClient httpClient() {
        return HttpClient.newBuilder().build();
    }

    @Bean
    public HttpRequest.Builder requestBuilder() {
        return HttpRequest.newBuilder();
    }
}
