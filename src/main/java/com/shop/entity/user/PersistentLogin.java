package com.shop.entity.user;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import static com.shop.config.security.SecurityConfig.PASSWORD_ENCODER;

@Entity
@Table(name = "persistent_login")
public class PersistentLogin implements Serializable {

    @Id
    private String series;

    @Column(name = "username", nullable = false, unique = true)
    @NaturalId
    private String username;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    private UserData userData;

    @Column(name = "token", nullable = false)
    private String token;

    @Column(name = "last_used", nullable = false)
    private LocalDateTime lastUsed;

    public PersistentLogin() {}

    public PersistentLogin(String username, String series, String token, LocalDateTime lastUsed) {
        this.series = series;
        this.username = username;
        setToken(token);
        this.lastUsed = lastUsed;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = PASSWORD_ENCODER.encode(token);
    }

    public LocalDateTime getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(LocalDateTime lastUsed) {
        this.lastUsed = lastUsed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersistentLogin that = (PersistentLogin) o;
        return series.equals(that.series) &&
                username.equals(that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(series, username);
    }
}
