package com.shop.entity.stock;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.shop.entity.Status;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "stock_order")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class StockOrder {

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(mappedBy = "stockOrder", fetch = FetchType.LAZY)
    @JsonIgnoreProperties("stockOrder")
    @OrderBy("created")
    private Set<StockOrderItem> stockOrderItems;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "stock_id", updatable = false, insertable = false)
    private Stock stock;

    @Column(name = "stock_id", nullable = false)
    private Long stockId;

    @Column(name = "sum", nullable = false)
    @ColumnDefault("0")
    private BigDecimal sum;

    @Column(name = "weight", nullable = false)
    @ColumnDefault("0")
    private BigDecimal weight;

    @Column(name = "pcs", nullable = false)
    @ColumnDefault("0")
    private Integer pcs;

    @Column(name = "volume", nullable = false)
    @ColumnDefault("0")
    private BigDecimal volume;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status.Stock status;

    @CreationTimestamp
    @Column(name = "created", nullable = false)
    private LocalDateTime created;

    @UpdateTimestamp
    @Column(name = "updated", nullable = false)
    private LocalDateTime updated;

    public BigDecimal getVolume() {
        return volume;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public Integer getPcs() {
        return pcs;
    }

    public void setPcs(Integer pcs) {
        this.pcs = pcs;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<StockOrderItem> getStockOrderItems() {
        return stockOrderItems;
    }

    public void setStockOrderItems(Set<StockOrderItem> stockOrderItems) {
        this.stockOrderItems = stockOrderItems;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Status.Stock getStatus() {
        return status;
    }

    public void setStatus(Status.Stock status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockOrder that = (StockOrder) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
