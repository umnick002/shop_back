package com.shop.entity.stock;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.shop.entity.delivery.DeliveryZone;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "stock")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Stock {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @Column(name = "lon", nullable = false)
    private Double lon;

    @Column(name = "lat", nullable = false)
    private Double lat;

    @OneToMany(mappedBy = "stock", fetch = FetchType.LAZY)
    @JsonIgnoreProperties("stock")
    private Set<DeliveryZone> deliveryZones;

    @OneToMany(mappedBy = "stock", fetch = FetchType.LAZY)
    @JsonIgnoreProperties("stock")
    private Set<StockReserve> stockReserves;

    public Set<DeliveryZone> getDeliveryZones() {
        return deliveryZones;
    }

    public void setDeliveryZones(Set<DeliveryZone> deliveryZones) {
        this.deliveryZones = deliveryZones;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<StockReserve> getStockReserves() {
        return stockReserves;
    }

    public void setStockReserves(Set<StockReserve> stockReserves) {
        this.stockReserves = stockReserves;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stock stock = (Stock) o;
        return Objects.equals(name, stock.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
