package com.shop.entity.stock;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shop.entity.product.price.BuyPrice;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "stock_order_item",
        uniqueConstraints = @UniqueConstraint(columnNames = {"stock_order_id", "buy_price_id"}))
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class StockOrderItem {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "stock_order_id", updatable = false, insertable = false)
    private StockOrder stockOrder;

    @Column(name = "stock_order_id", nullable = false)
    private Long stockOrderId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "buy_price_id", nullable = false)
    private BuyPrice buyPrice;

    @Column(name = "count", nullable = false)
    private Integer count;

    @Column(name = "created", nullable = false)
    @CreationTimestamp
    private LocalDateTime created;

    @Column(name = "updated", nullable = false)
    @UpdateTimestamp
    private LocalDateTime updated;

    @Transient
    @JsonProperty("totalPrice")
    private BigDecimal totalPrice;

    public Long getStockOrderId() {
        return stockOrderId;
    }

    public void setStockOrderId(Long stockOrderId) {
        this.stockOrderId = stockOrderId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StockOrder getStockOrder() {
        return stockOrder;
    }

    public void setStockOrder(StockOrder stockOrder) {
        this.stockOrder = stockOrder;
    }

    public BuyPrice getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BuyPrice buyPrice) {
        this.buyPrice = buyPrice;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockOrderItem that = (StockOrderItem) o;
        return stockOrderId.equals(that.stockOrderId) &&
                buyPrice.equals(that.buyPrice) && count.equals(that.count);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stockOrderId, buyPrice, count);
    }
}
