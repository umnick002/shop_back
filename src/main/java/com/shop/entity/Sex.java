package com.shop.entity;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Sex {
    M("м"), F("ж");
    private String value;

    Sex(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return value;
    }
}
