package com.shop.entity;

import com.fasterxml.jackson.annotation.JsonValue;

public enum  MimeType {
    JPEG("image/jpeg", ".jpg");

    private String value;
    private String suffix;

    MimeType(String value, String suffix) {
        this.value = value;
        this.suffix = suffix;
    }

    public String getSuffix() {
        return suffix;
    }

    @Override
    @JsonValue
    public String toString() {
        return value;
    }
}
