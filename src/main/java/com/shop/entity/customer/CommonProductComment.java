package com.shop.entity.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shop.entity.product.description.CommonProduct;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "common_product_comment")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CommonProductComment {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", updatable = false, insertable = false)
    @JsonIgnoreProperties({"commonProductComments", "customerFavorites", "addresses"})
    private Customer customer;

    @Column(name = "customer_id", nullable = false)
    private Long customerId;

    @Column(name = "common_product_id", nullable = false)
    private Long commonProductId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "common_product_id", insertable = false, updatable = false)
    private CommonProduct commonProduct;

    @Column(name = "comment")
    private String comment;

    @Column(name = "created", nullable = false)
    private LocalDateTime created;

    @Column(name = "rating", nullable = false)
    @Min(1)
    @Max(5)
    private Short rating;

    @Transient
    @JsonProperty("isAuthor")
    private Boolean isAuthor = false;

    public Boolean getAuthor() {
        return isAuthor;
    }

    public void setAuthor(Boolean author) {
        isAuthor = author;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setCommonProductId(Long commonProductId) {
        this.commonProductId = commonProductId;
    }

    public Long getCommonProductId() {
        return commonProductId;
    }

    public CommonProduct getCommonProduct() {
        return commonProduct;
    }

    public void setCommonProduct(CommonProduct commonProduct) {
        this.commonProduct = commonProduct;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Short getRating() {
        return rating;
    }

    public void setRating(Short rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommonProductComment that = (CommonProductComment) o;
        return customerId.equals(that.customerId) &&
                commonProductId.equals(that.commonProductId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerId, commonProductId);
    }
}
