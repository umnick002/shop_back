package com.shop.entity.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shop.entity.product.price.SellPrice;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "customer_order_item")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomerOrderItem {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_order_id", insertable = false, updatable = false)
    @JsonIgnoreProperties("customerOrderItems")
    private CustomerOrder customerOrder;

    @Column(name = "customer_order_id", nullable = false)
    private Long customerOrderId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sell_price_id", insertable = false, updatable = false)
    @JsonIgnoreProperties("customerOrderItems")
    private SellPrice sellPrice;

    @Column(name = "sell_price_id", nullable = false)
    private Long sellPriceId;

    @Transient
    @JsonProperty("actualSellPrice")
    private SellPrice actualSellPrice;

    @Column(name = "count", nullable = false)
    @ColumnDefault("1")
    @Min(1)
    private Integer count;

    @Column(name = "created")
    @CreationTimestamp
    private LocalDateTime created;

    @Column(name = "updated")
    @UpdateTimestamp
    private LocalDateTime updated;

    @Column(name = "sum", nullable = false)
    @ColumnDefault("0")
    @Min(0)
    private BigDecimal sum;

    @Column(name = "weight", nullable = false)
    @ColumnDefault("0")
    @Min(0)
    private BigDecimal weight;

    @Column(name = "volume", nullable = false)
    @ColumnDefault("0")
    @Min(0)
    private BigDecimal volume;

    public Long getCustomerOrderId() {
        return customerOrderId;
    }

    public void setCustomerOrderId(Long customerOrderId) {
        this.customerOrderId = customerOrderId;
    }

    public Long getSellPriceId() {
        return sellPriceId;
    }

    public void setSellPriceId(Long sellPriceId) {
        this.sellPriceId = sellPriceId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SellPrice getActualSellPrice() {
        return actualSellPrice;
    }

    public void setActualSellPrice(SellPrice actualSellPrice) {
        this.actualSellPrice = actualSellPrice;
    }

    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    public void setCustomerOrder(CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
        if (customerOrder.getId() != null) {
            this.customerOrderId = customerOrder.getId();
        }
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public SellPrice getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(SellPrice sellPrice) {
        this.sellPrice = sellPrice;
        if (sellPrice.getId() != null) {
            this.sellPriceId = sellPrice.getId();
        }
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerOrderItem that = (CustomerOrderItem) o;
        return customerOrderId.equals(that.customerOrderId) &&
                sellPriceId.equals(that.sellPriceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerOrderId, sellPriceId);
    }
}
