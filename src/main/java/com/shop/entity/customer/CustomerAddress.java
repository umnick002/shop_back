package com.shop.entity.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.shop.entity.delivery.DeliveryZone;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "customer_address")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomerAddress {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", updatable = false, insertable = false)
    @JsonIgnoreProperties({"commonProductComments", "customerFavorites", "addresses"})
    private Customer customer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "delivery_zone_id", nullable = false)
    private DeliveryZone deliveryZone;

    @Column(name = "customer_id", nullable = false)
    private Long customerId;

    @Column(name = "lon", nullable = false)
    private Double lon;

    @Column(name = "lat", nullable = false)
    private Double lat;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "street", nullable = false)
    private String street;

    @Column(name = "house", nullable = false)
    private String house;

    @Column(name = "building")
    private String building;

    @Column(name = "frame")
    private String frame;

    @Column(name = "entrance")
    private String entrance;

    @Column(name = "floor")
    private Short floor;

    @Column(name = "flat")
    private String flat;

    @Column(name = "is_flat", nullable = false)
    private Boolean isFlat;

    @Column(name = "retired", nullable = false)
    private Boolean retired;

    @Column(name = "created")
    @CreationTimestamp
    private LocalDateTime created;

    @Column(name = "updated")
    @UpdateTimestamp
    private LocalDateTime updated;

    @OneToMany(mappedBy = "address", fetch = FetchType.LAZY)
    @JsonIgnoreProperties("address")
    private Set<CustomerOrder> customerOrders;

    public DeliveryZone getDeliveryZone() {
        return deliveryZone;
    }

    public void setDeliveryZone(DeliveryZone deliveryZone) {
        this.deliveryZone = deliveryZone;
    }

    public Set<CustomerOrder> getCustomerOrders() {
        return customerOrders;
    }

    public void setCustomerOrders(Set<CustomerOrder> customerOrders) {
        this.customerOrders = customerOrders;
    }

    public Boolean getRetired() {
        return retired;
    }

    public void setRetired(Boolean retired) {
        this.retired = retired;
    }

    public void setFlat(Boolean flat) {
        isFlat = flat;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getFrame() {
        return frame;
    }

    public void setFrame(String frame) {
        this.frame = frame;
    }

    public String getEntrance() {
        return entrance;
    }

    public void setEntrance(String entrance) {
        this.entrance = entrance;
    }

    public Short getFloor() {
        return floor;
    }

    public void setFloor(Short floor) {
        this.floor = floor;
    }

    public String getFlat() {
        return flat;
    }

    public Boolean getIsFlat() {
        return isFlat;
    }

    public void setIsFlat(Boolean flat) {
        isFlat = flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerAddress that = (CustomerAddress) o;
        return customerId.equals(that.customerId) &&
                lon.equals(that.lon) &&
                lat.equals(that.lat) &&
                city.equals(that.city) &&
                street.equals(that.street) &&
                house.equals(that.house) &&
                Objects.equals(building, that.building) &&
                Objects.equals(frame, that.frame) &&
                Objects.equals(entrance, that.entrance) &&
                Objects.equals(floor, that.floor) &&
                Objects.equals(flat, that.flat) &&
                Objects.equals(isFlat, that.isFlat) &&
                Objects.equals(retired, that.retired);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerId, lon, lat, city, street, house, building, frame, entrance, floor, flat, isFlat, retired);
    }
}
