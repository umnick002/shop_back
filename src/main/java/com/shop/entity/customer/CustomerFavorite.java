package com.shop.entity.customer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shop.entity.product.description.CommonProduct;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "customer_favorite")
@IdClass(CustomerFavoritePK.class)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomerFavorite implements Serializable {

    @Column(name = "favorite_id", nullable = false, updatable = false, insertable = false)
    private Long favoriteId;

    @Column(name = "customer_id", nullable = false, updatable = false, insertable = false)
    private Long customerId;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "favorite_id", nullable = false)
    private CommonProduct favorite;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @Transient
    @JsonProperty("isFavorite")
    private Boolean isFavorite;

    public CustomerFavorite() {}

    public CustomerFavorite(Customer customer, CommonProduct favorite) {
        this.customer = customer;
        this.favorite = favorite;
    }

    public Boolean getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(Boolean favorite) {
        isFavorite = favorite;
    }

    public Long getFavoriteId() {
        return favoriteId;
    }

    public void setFavoriteId(Long favoriteId) {
        this.favoriteId = favoriteId;
    }

    public CommonProduct getFavorite() {
        return favorite;
    }

    public void setFavorite(CommonProduct favorite) {
        this.favorite = favorite;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerFavorite that = (CustomerFavorite) o;
        return favorite.equals(that.favorite) &&
                customer.equals(that.customer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(favorite, customer);
    }
}
