package com.shop.entity.customer;

import java.io.Serializable;
import java.util.Objects;

public class CustomerFavoritePK implements Serializable {

    private Long favorite;
    private Long customer;

    public CustomerFavoritePK() {}

    public CustomerFavoritePK(Long favorite, Long customer) {
        this.favorite = favorite;
        this.customer = customer;
    }

    public Long getFavorite() {
        return favorite;
    }

    public void setFavorite(Long favorite) {
        this.favorite = favorite;
    }

    public Long getCustomer() {
        return customer;
    }

    public void setCustomer(Long customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerFavoritePK that = (CustomerFavoritePK) o;
        return favorite.equals(that.favorite) &&
                customer.equals(that.customer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(favorite, customer);
    }
}
