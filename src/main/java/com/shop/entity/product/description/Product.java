package com.shop.entity.product.description;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.shop.entity.Unit;
import com.shop.entity.product.price.BuyPrice;
import com.shop.entity.product.price.SellPrice;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "product",
        uniqueConstraints = @UniqueConstraint(columnNames = {"pack_volume", "common_product_id"}))
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "pack_volume", nullable = false)
    private BigDecimal packVolume;

    @Enumerated(EnumType.STRING)
    @Column(name = "pack_volume_unit", nullable = false, length = 16)
    private Unit packVolumeUnit;

    @Column(name = "weight", nullable = false)
    private BigDecimal weight;

    @Column(name = "length", nullable = false)
    private BigDecimal length;

    @Column(name = "width", nullable = false)
    private BigDecimal width;

    @Column(name = "height", nullable = false)
    private BigDecimal height;

    @Column(name = "number")
    private Long number;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
    @JsonIgnoreProperties({"product"})
    @OrderBy("created DESC")
    private Set<SellPrice> sellPrices;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
    @JsonIgnoreProperties({"product"})
    private Set<BuyPrice> buyPrices;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "common_product_id", insertable = false, updatable = false)
    @JsonIgnoreProperties("products")
    private CommonProduct commonProduct;

    @Column(name = "common_product_id", nullable = false)
    private Long commonProductId;

    @Transient
    @JsonProperty("reserved")
    private Boolean reserved = null;

    public Boolean getReserved() {
        return reserved;
    }

    public void setReserved(Boolean reserved) {
        this.reserved = reserved;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getHeight() {
        return height;
    }

    public void setHeight(BigDecimal height) {
        this.height = height;
    }

    public BigDecimal getLength() {
        return length;
    }

    public void setLength(BigDecimal length) {
        this.length = length;
    }

    public BigDecimal getWidth() {
        return width;
    }

    public void setWidth(BigDecimal width) {
        this.width = width;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Set<BuyPrice> getBuyPrices() {
        return buyPrices;
    }

    public void setSellPrices(Set<SellPrice> sellPrices) {
        this.sellPrices = sellPrices;
    }

    public Set<SellPrice> getSellPrices() {
        return sellPrices;
    }

    public void setBuyPrices(Set<BuyPrice> buyPrices) {
        this.buyPrices = buyPrices;
    }

    public CommonProduct getCommonProduct() {
        return commonProduct;
    }

    public void setCommonProduct(CommonProduct commonProduct) {
        this.commonProduct = commonProduct;
    }

    public BigDecimal getPackVolume() {
        return packVolume;
    }

    public void setPackVolume(BigDecimal packVolume) {
        this.packVolume = packVolume;
    }

    public Unit getPackVolumeUnit() {
        return packVolumeUnit;
    }

    public void setPackVolumeUnit(Unit packVolumeUnit) {
        this.packVolumeUnit = packVolumeUnit;
    }

    public Long getCommonProductId() {
        return commonProductId;
    }

    public void setCommonProductId(Long commonProductId) {
        this.commonProductId = commonProductId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return packVolume.equals(product.packVolume) &&
                commonProductId.equals(product.commonProductId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(packVolume, commonProductId);
    }
}
