package com.shop.entity.product.description;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Table(name = "sub_category",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"category_id", "priority"})})
public class SubCategory implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @NaturalId
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "priority", nullable = false)
    private Short priority;

    @OneToMany(mappedBy = "subCategory")
    @JsonIgnoreProperties("subCategory")
    private Set<CommonProduct> commonProducts;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id", nullable = false)
    @JsonIgnoreProperties("subCategories")
    private Category category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getPriority() {
        return priority;
    }

    public void setPriority(Short priority) {
        this.priority = priority;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<CommonProduct> getCommonProducts() {
        return commonProducts;
    }

    public void setCommonProducts(Set<CommonProduct> commonProducts) {
        this.commonProducts = commonProducts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubCategory that = (SubCategory) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
