package com.shop.entity.product.description;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.shop.entity.MimeType;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.Arrays;

@Entity
@Table(name = "content", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"hash"})
})
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Content {

    @Id
    @GeneratedValue
    private Long id;

    @NaturalId
    @Column(name = "file", nullable = false, unique = true)
    private String file;

    @Column(name = "mime_type", nullable = false, length = 16)
    @Enumerated(EnumType.STRING)
    private MimeType mimeType;

    @Column(name = "priority", nullable = false)
    private Short priority;

    @Column(name = "hash", nullable = false, unique = true)
    private byte[] hash;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("contents")
    @JoinColumn(name = "common_product_id", insertable = false, updatable = false)
    private CommonProduct commonProduct;

    @Column(name = "common_product_id")
    private Long commonProductId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getHash() {
        return hash;
    }

    public void setHash(byte[] hash) {
        this.hash = hash;
    }

    public CommonProduct getCommonProduct() {
        return commonProduct;
    }

    public void setCommonProduct(CommonProduct commonProduct) {
        this.commonProduct = commonProduct;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public MimeType getMimeType() {
        return mimeType;
    }

    public void setMimeType(MimeType mimeType) {
        this.mimeType = mimeType;
    }

    public Short getPriority() {
        return priority;
    }

    public void setPriority(Short priority) {
        this.priority = priority;
    }

    public void setCommonProductId(Long commonProductId) {
        this.commonProductId = commonProductId;
    }

    public Long getCommonProductId() {
        return commonProductId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Content content = (Content) o;
        return Arrays.equals(hash, content.hash);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(hash);
    }
}
