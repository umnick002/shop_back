package com.shop.entity.product.description;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "property", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"key", "common_product_id"})
})
public class Property {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "key", nullable = false)
    private PropertyDictionary key;

    @Column(name = "value", nullable = false)
    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "common_product_id", updatable = false, insertable = false)
    @JsonIgnoreProperties("properties")
    private CommonProduct commonProduct;

    @Column(name = "common_product_id", nullable = false)
    private Long commonProductId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PropertyDictionary getKey() {
        return key;
    }

    public void setKey(PropertyDictionary key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public CommonProduct getCommonProduct() {
        return commonProduct;
    }

    public void setCommonProduct(CommonProduct commonProduct) {
        this.commonProduct = commonProduct;
    }

    public Long getCommonProductId() {
        return commonProductId;
    }

    public void setCommonProductId(Long commonProductId) {
        this.commonProductId = commonProductId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Property property = (Property) o;
        return Objects.equals(key, property.key) &&
                Objects.equals(commonProductId, property.commonProductId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, commonProductId);
    }
}
