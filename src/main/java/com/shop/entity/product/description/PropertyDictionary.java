package com.shop.entity.product.description;

import org.hibernate.annotations.NaturalId;
import org.springframework.data.annotation.Immutable;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "property_dictionary")
@Immutable
public class PropertyDictionary {

    @Id
    @GeneratedValue
    private Long id;

    @NaturalId
    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @Column(name = "priority", unique = true, nullable = false)
    private Integer priority;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PropertyDictionary that = (PropertyDictionary) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
