package com.shop.entity.product.price;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.shop.entity.customer.CustomerOrderItem;
import com.shop.entity.product.description.Product;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "sell_price")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SellPrice {

    public SellPrice() {}

    public SellPrice(BuyPrice buyPrice) {
        this.setProductId(buyPrice.getProductId());
        this.setPrice(buyPrice.getPrice().multiply(BigDecimal.valueOf(1.15)));
        this.setPricePerUnit(this.getPrice()
                .multiply(buyPrice.getPricePerUnit())
                .divide(buyPrice.getPrice(), MathContext.DECIMAL32)
        );
    }

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    @Column(name = "price", nullable = false)
    private BigDecimal price;

    @Column(name = "price_per_unit", nullable = false)
    private BigDecimal pricePerUnit;

    @Column(name = "created", nullable = false)
    @CreationTimestamp
    private LocalDateTime created;

    @OneToMany(mappedBy = "sellPrice", fetch = FetchType.LAZY)
    @JsonIgnoreProperties("sellPrice")
    private Set<CustomerOrderItem> customerOrderItems;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JsonIgnoreProperties("sellPrices")
    @JoinColumn(name = "product_id", insertable = false, updatable = false)
    private Product product;

    @Column(name = "product_id")
    private Long productId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(BigDecimal pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Set<CustomerOrderItem> getCustomerOrderItems() {
        return customerOrderItems;
    }

    public void setCustomerOrderItems(Set<CustomerOrderItem> customerOrderItems) {
        this.customerOrderItems = customerOrderItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SellPrice sellPrice = (SellPrice) o;
        return price.equals(sellPrice.price) &&
                productId.equals(sellPrice.productId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, productId);
    }
}
