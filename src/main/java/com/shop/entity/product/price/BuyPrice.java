package com.shop.entity.product.price;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.shop.entity.stock.StockOrderItem;
import com.shop.entity.product.description.Product;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "buy_price")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BuyPrice {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "price", nullable = false)
    private BigDecimal price;

    @Column(name = "price_per_unit", nullable = false)
    private BigDecimal pricePerUnit;

    @Column(name = "created", nullable = false)
    @CreationTimestamp
    private LocalDateTime created;

    @OneToMany(mappedBy = "buyPrice")
    @JsonIgnoreProperties("buyPrice")
    private Set<StockOrderItem> stockOrderItems;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "product_id", insertable = false, updatable = false, nullable = false)
    @JsonIgnoreProperties("buyPrices")
    private Product product;

    @Column(name = "product_id")
    private Long productId;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(BigDecimal pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Set<StockOrderItem> getStockOrderItems() {
        return stockOrderItems;
    }

    public void setStockOrderItems(Set<StockOrderItem> stockOrderItems) {
        this.stockOrderItems = stockOrderItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BuyPrice buyPrice = (BuyPrice) o;
        return price.equals(buyPrice.price) &&
                productId.equals(buyPrice.productId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, productId);
    }
}
