package com.shop.entity;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Unit {
    KG("кг"), L("л"), PRICE_KG("р/кг"), PRICE_L("р/л"), PRICE_PACK("р/шт");
    private String value;

    Unit(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return value;
    }
}
