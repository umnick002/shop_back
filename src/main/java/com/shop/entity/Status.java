package com.shop.entity;

import com.fasterxml.jackson.annotation.JsonValue;

public class Status {
    public enum Stock {
        DRAFT("Оформляется"), FINALIZE("Оформлено"), ORDER("Заказано"), DELIVER("Доставлено"),
        COMPLETE("Выполнено"), CANCEL("Отменено");

        private String value;

        Stock(String value) {
            this.value = value;
        }

        @Override
        @JsonValue
        public String toString() {
            return value;
        }
    }

    public enum Customer {
        DRAFT("Оформляется"), ORDER("Заказано"), PREPARE("Собирается"),
        READY("Готов к отправке"), SEND("Отправлено"), DELIVER("Доставлено"),
        COMPLETE("Выполнено"), CANCEL("Отменено");

        private String value;

        Customer(String value) {
            this.value = value;
        }

        @Override
        @JsonValue
        public String toString() {
            return value;
        }
    }
}
