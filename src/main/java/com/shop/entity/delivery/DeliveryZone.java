package com.shop.entity.delivery;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.shop.entity.stock.Stock;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "delivery_zone")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DeliveryZone {

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(mappedBy = "deliveryZone", fetch = FetchType.LAZY)
    @JsonIgnoreProperties("deliveryZone")
    @OrderBy("after desc")
    private Set<DeliveryZoneTime> deliveryZoneTimes;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("deliveryZone")
    @JoinColumn(name = "stock_id", nullable = false)
    private Stock stock;

    @Column(name = "days", nullable = false)
    @ElementCollection
    @Enumerated(EnumType.STRING)
    private Set<DayOfWeek> days;

    @Column(name = "lon", nullable = false)
    private Double lon;

    @Column(name = "lat", nullable = false)
    private Double lat;

    @Column(name = "min_duration", nullable = false)
    private Double minDuration;

    @Column(name = "max_duration", nullable = false)
    private Double maxDuration;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<DeliveryZoneTime> getDeliveryZoneTimes() {
        return deliveryZoneTimes;
    }

    public void setDeliveryZoneTimes(Set<DeliveryZoneTime> deliveryZoneTimes) {
        this.deliveryZoneTimes = deliveryZoneTimes;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Set<DayOfWeek> getDays() {
        return days;
    }

    public void setDays(Set<DayOfWeek> days) {
        this.days = days;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getMinDuration() {
        return minDuration;
    }

    public void setMinDuration(Double minDuration) {
        this.minDuration = minDuration;
    }

    public Double getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(Double maxDuration) {
        this.maxDuration = maxDuration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeliveryZone that = (DeliveryZone) o;
        return lon.equals(that.lon) &&
                lat.equals(that.lat) &&
                minDuration.equals(that.minDuration) &&
                maxDuration.equals(that.maxDuration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lon, lat, minDuration, maxDuration);
    }
}
