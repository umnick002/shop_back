package com.shop.entity.delivery;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.shop.entity.customer.CustomerOrder;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "delivery_point")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DeliveryPoint {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "delivery_day")
    private LocalDate deliveryDay;

    @Column(name = "position")
    private Short position;

    @Column(name = "duration")
    private Double duration;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_order_id", nullable = false)
    @JsonIgnoreProperties({"customerOrder"})
    private CustomerOrder customerOrder;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "delivery_zone_time_id")
    private DeliveryZoneTime deliveryZoneTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDeliveryDay() {
        return deliveryDay;
    }

    public void setDeliveryDay(LocalDate deliveryDay) {
        this.deliveryDay = deliveryDay;
    }

    public Short getPosition() {
        return position;
    }

    public void setPosition(Short position) {
        this.position = position;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    public void setCustomerOrder(CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
    }

    public DeliveryZoneTime getDeliveryZoneTime() {
        return deliveryZoneTime;
    }

    public void setDeliveryZoneTime(DeliveryZoneTime deliveryZoneTime) {
        this.deliveryZoneTime = deliveryZoneTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeliveryPoint that = (DeliveryPoint) o;
        return Objects.equals(deliveryDay, that.deliveryDay) &&
                Objects.equals(position, that.position) &&
                Objects.equals(deliveryZoneTime, that.deliveryZoneTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deliveryDay, position, deliveryZoneTime);
    }
}
