package com.shop.entity.delivery;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "delivery_zone_time")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DeliveryZoneTime {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "after")
    private LocalTime after;

    @Column(name = "before")
    private LocalTime before;

    @Column(name = "weight")
    private Double weight;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "zone_id")
    private DeliveryZone deliveryZone;

    @Transient
    @JsonProperty("load")
    private Load load = Load.LOW;

    private enum Load {
        LOW, MID, HIGH
    }

    public Load getLoad() {
        return load;
    }

    public void setLoad(Load load) {
        this.load = load;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalTime getAfter() {
        return after;
    }

    public void setAfter(LocalTime after) {
        this.after = after;
    }

    public LocalTime getBefore() {
        return before;
    }

    public void setBefore(LocalTime before) {
        this.before = before;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public DeliveryZone getDeliveryZone() {
        return deliveryZone;
    }

    public void setDeliveryZone(DeliveryZone deliveryZone) {
        this.deliveryZone = deliveryZone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeliveryZoneTime that = (DeliveryZoneTime) o;
        return after.equals(that.after) &&
                before.equals(that.before) &&
                deliveryZone.equals(that.deliveryZone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(after, before, deliveryZone);
    }
}
