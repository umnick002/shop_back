package com.shop.controller;

import com.shop.entity.customer.CustomerOrder;
import com.shop.entity.customer.CustomerOrderItem;
import com.shop.entity.user.UserData;
import com.shop.service.CustomerOrderService;
import com.shop.service.DeliveryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value="/order")
@Secured("ROLE_CUSTOMER")
public class CustomerOrderController {

    private static Logger LOGGER = LoggerFactory.getLogger(CustomerOrderController.class);

    private final CustomerOrderService customerOrderService;
    private final DeliveryService deliveryService;

    @Autowired
    public CustomerOrderController(CustomerOrderService customerOrderService, DeliveryService deliveryService) {
        this.customerOrderService = customerOrderService;
        this.deliveryService = deliveryService;
    }

    @RequestMapping(value = "/item", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CustomerOrderItem updateOrderItem(@RequestParam("product_id") Long productId,
                                             @RequestParam("count") Integer count,
                                             @AuthenticationPrincipal UserData user,
                                             HttpServletResponse response) {
        try {
            return customerOrderService.updateOrderItem(user, productId, count);
        } catch (Exception e) {
            LOGGER.error("Update order item failed", e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CustomerOrder getOrder(@AuthenticationPrincipal UserData user, HttpServletResponse response) {
        try {
            return customerOrderService.getOrder(user);
        } catch (Exception e) {
            LOGGER.error("Reserve order failed", e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }

    @RequestMapping(value = "/confirm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CustomerOrder updateOrderItem(@AuthenticationPrincipal UserData user, HttpServletResponse response) {
        try {
            return deliveryService.confirmOrder(user);
        } catch (Exception e) {
            LOGGER.error("Confirm order failed", e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }
}
