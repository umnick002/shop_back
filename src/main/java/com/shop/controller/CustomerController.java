package com.shop.controller;

import com.shop.entity.customer.Customer;
import com.shop.entity.customer.CustomerAddress;
import com.shop.entity.customer.CustomerPhone;
import com.shop.entity.delivery.DeliveryZone;
import com.shop.entity.user.UserData;
import com.shop.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@Secured("ROLE_CUSTOMER")
public class CustomerController {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @RequestMapping(value = "/favorites/{commonProductId}", method = RequestMethod.POST)
    public void addToFavorite(@PathVariable Long commonProductId) {
        customerService.addFavorite(commonProductId);
    }

    @RequestMapping(value = "/favorites/{commonProductId}", method = RequestMethod.DELETE)
    public void deleteFromFavorite(@PathVariable Long commonProductId) {
        customerService.deleteFavorite(commonProductId);
    }

    @RequestMapping(value = "/favorites/{commonProductId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Boolean getCustomerFavorite(@PathVariable Long commonProductId) {
        return customerService.getCustomerFavorite(commonProductId);
    }

    @RequestMapping(value = "/details", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Customer getDetails(@AuthenticationPrincipal UserData user) {
        return customerService.getDetails(user);
    }

    @RequestMapping(value = "/address", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Set<CustomerAddress> getAddresses() {
        return customerService.getAddresses();
    }

    @RequestMapping(value = "/address", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CustomerAddress addAddress(@RequestBody CustomerAddress address) {
        return customerService.addAddress(address);
    }

    @RequestMapping(value = "/address/check", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public DeliveryZone checkAddress(@RequestParam Double lon, @RequestParam Double lat) {
        return customerService.getDeliveryZone(lon, lat);
    }

    @RequestMapping(value = "/address", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CustomerAddress updateAddress(@RequestBody CustomerAddress address) {
        return customerService.updateAddress(address);
    }

    @RequestMapping(value = "/phone/verify", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CustomerPhone verifyPhone(@AuthenticationPrincipal UserData user, @RequestParam String phone, @RequestParam String code) {
        return customerService.verifyPhone(user, phone, code);
    }

    @RequestMapping(value = "/phone/code", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CustomerPhone sendVerifyingCode(@AuthenticationPrincipal UserData user, @RequestParam String phone) {
        return customerService.sendVerifyingCode(user, phone);
    }

    @RequestMapping(value = "/customer", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Customer updateCustomer(@AuthenticationPrincipal UserData user, @RequestBody Customer customer) {
        return customerService.updateCustomer(user, customer);
    }

    @RequestMapping(value = "/phones", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Set<CustomerPhone> getPhones(@AuthenticationPrincipal UserData user, @RequestParam boolean verified) {
        return customerService.getPhones(user, verified);
    }

}
