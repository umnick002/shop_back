package com.shop.controller;

import com.shop.entity.delivery.DeliveryPoint;
import com.shop.entity.delivery.DeliveryZoneTime;
import com.shop.entity.user.UserData;
import com.shop.service.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value="/delivery")
@Secured("ROLE_CUSTOMER")
public class DeliveryController {
    private final DeliveryService deliveryService;

    @Autowired
    public DeliveryController(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    @RequestMapping(value = "/days", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<LocalDate> getDeliveryDays(@AuthenticationPrincipal UserData user, @RequestParam("address_id") Long customerAddressId) {
        return deliveryService.getDeliveryDays(user, customerAddressId);
    }

    @RequestMapping(value = "/times", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<DeliveryZoneTime> getDeliveryTimes(@AuthenticationPrincipal UserData user,
                                                  @RequestParam("address_id") Long customerAddressId, @RequestParam("day") String dateStr) {
        return deliveryService.getDeliveryTimes(user, customerAddressId, dateStr);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public DeliveryPoint createDeliveryPoint(@AuthenticationPrincipal UserData user,
                                             @RequestParam("address_id") Long customerAddressId,
                                             @RequestParam("day") String dateStr,
                                             @RequestParam("after") String after, @RequestParam("before") String before) {
        return deliveryService.createDeliveryPoint(user, customerAddressId, dateStr, after, before);
    }
}
