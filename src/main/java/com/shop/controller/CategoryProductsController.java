package com.shop.controller;

import com.shop.entity.product.description.CommonProduct;
import com.shop.service.CommonProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(value="/categories")
public class CategoryProductsController {

    private final CommonProductService commonProductService;

    @Autowired
    public CategoryProductsController(CommonProductService commonProductService) {
        this.commonProductService = commonProductService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Set<CommonProduct> getProduct(@PathVariable("id") Long id) {
        if (id%100 != 0) {
            return commonProductService.getCommonProductsByCategory(id/100, id%100);
        } else {
            return commonProductService.getCommonProductsByCategory(id/100);
        }
    }
}
