package com.shop.controller;

import com.shop.entity.Status;
import com.shop.entity.stock.StockOrder;
import com.shop.service.StockOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Set;

@RestController
@RequestMapping(value="/stock/orders")
public class StockOrderController {

    private static Logger LOGGER = LoggerFactory.getLogger(StockOrderController.class);

    private final StockOrderService stockOrderService;

    @Autowired
    public StockOrderController(StockOrderService stockOrderService) {
        this.stockOrderService = stockOrderService;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public StockOrder updateOrder(@RequestBody StockOrder order, HttpServletResponse response) {
        try {
            if (order.getId() == null) {
                return stockOrderService.createOrder(order);
            } else {
                return stockOrderService.updateOrder(order);
            }
        } catch (Exception e) {
            LOGGER.error("Update order failed", e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/{orderId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public StockOrder changeStatus(@PathVariable("orderId") Long orderId, @RequestParam("status") Status.Stock status,
                                   HttpServletResponse response) {
        try {
            return stockOrderService.changeStatus(orderId, status);
        } catch (Exception e) {
            LOGGER.error("Update order failed", e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Set<StockOrder> getOrders(@RequestParam("page") Short page, @RequestParam("statuses") Set<Status.Stock> statuses) {
        return stockOrderService.getStockOrders(page, statuses);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public StockOrder getOrder(@PathVariable Long id) {
        return stockOrderService.getOrder(id);
    }
}
