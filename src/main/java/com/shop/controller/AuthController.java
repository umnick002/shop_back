package com.shop.controller;

import com.shop.config.security.CustomUserDetailsManager;
import com.shop.entity.customer.Customer;
import com.shop.entity.user.UserData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
public class AuthController {

    private static Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    private final CustomUserDetailsManager userDetailsManager;

    @Autowired
    public AuthController(CustomUserDetailsManager userDetailsManager) {
        this.userDetailsManager = userDetailsManager;
    }

    @RequestMapping(value="/registration", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserData registration(@RequestBody Customer customer, HttpServletResponse response) {
        try {
            return userDetailsManager.handleCreateCustomer(customer);
        } catch (Exception e) {
            LOGGER.error("Registration failed", e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }
}
