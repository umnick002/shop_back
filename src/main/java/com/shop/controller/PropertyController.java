package com.shop.controller;

import com.shop.entity.product.description.PropertyDictionary;
import com.shop.repository.product.description.PropertyDictionaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(value="/property-list")
public class PropertyController {

    private final PropertyDictionaryRepository propertyDictionaryRepository;

    @Autowired
    public PropertyController(PropertyDictionaryRepository propertyDictionaryRepository) {
        this.propertyDictionaryRepository = propertyDictionaryRepository;
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Set<PropertyDictionary> getProperties() {
        Set<PropertyDictionary> propertyDictionaries = new HashSet<>();
        propertyDictionaryRepository.findAll().forEach(propertyDictionaries::add);
        return propertyDictionaries;
    }
}
