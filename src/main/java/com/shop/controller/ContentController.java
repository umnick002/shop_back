package com.shop.controller;

import com.shop.entity.product.description.Content;
import com.shop.service.CommonProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

@RestController
@RequestMapping(value="/contents")
public class ContentController {

    private static Logger LOGGER = LoggerFactory.getLogger(ContentController.class);

    private final CommonProductService commonProductService;

    @Autowired
    public ContentController(CommonProductService commonProductService) {
        this.commonProductService = commonProductService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Secured("ROLE_ADMIN")
    public Content addContent(@RequestParam("id") Long commonProductId, @RequestBody byte[] data,
                              HttpServletRequest request, HttpServletResponse response) {
        try {
            return commonProductService.addContent(commonProductId, request.getHeader(CONTENT_TYPE), data);
        } catch (Exception e) {
            LOGGER.error("Save file failed", e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }
}
