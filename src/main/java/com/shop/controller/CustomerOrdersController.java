package com.shop.controller;

import com.shop.entity.Status;
import com.shop.entity.customer.CustomerOrder;
import com.shop.entity.user.UserData;
import com.shop.service.CustomerOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Set;

@RestController
@RequestMapping(value="/orders")
@Secured("ROLE_ADMIN")
public class CustomerOrdersController {

    private static Logger LOGGER = LoggerFactory.getLogger(CustomerOrdersController.class);

    private final CustomerOrderService customerOrderService;

    @Autowired
    public CustomerOrdersController(CustomerOrderService customerOrderService) {
        this.customerOrderService = customerOrderService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Set<CustomerOrder> getOrders(@RequestParam("page") Short page, @RequestParam("statuses") Set<Status.Customer> statuses) {
        return customerOrderService.getOrders(page, statuses);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CustomerOrder getOrder(@PathVariable("id") Long id) {
        return customerOrderService.getOrder(id);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CustomerOrder updateOrder(@RequestBody CustomerOrder order, @AuthenticationPrincipal UserData user,
                                     HttpServletResponse response) {
        try {
            return null;
        } catch (Exception e) {
            LOGGER.error("Update order failed", e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }
}
