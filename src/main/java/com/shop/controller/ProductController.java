package com.shop.controller;

import com.shop.entity.customer.CommonProductComment;
import com.shop.entity.product.description.CommonProduct;
import com.shop.service.CommonProductService;
import com.shop.wrapper.CommonProductsWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Set;

@RestController
@RequestMapping(value="/products")
public class ProductController {

    private static Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    private final CommonProductService commonProductService;

    @Autowired
    public ProductController(CommonProductService commonProductService) {
        this.commonProductService = commonProductService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CommonProduct getProduct(@PathVariable Long id) {
        return commonProductService.getCommonProduct(id);
    }

    @Secured("ROLE_CUSTOMER")
    @RequestMapping(value = "/{id}/comments", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CommonProductComment addComment(@PathVariable Long id, @RequestBody CommonProductComment commonProductComment) {
        return commonProductService.addComment(id, commonProductComment.getComment(), commonProductComment.getRating());
    }

    @RequestMapping(value = "/{id}/comments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Set<CommonProductComment> getComments(@PathVariable Long id) {
        return commonProductService.getComments(id);
    }

    @RequestMapping(value="", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Secured("ROLE_ADMIN")
    public CommonProduct saveCommonProduct(@RequestBody CommonProduct commonProduct, HttpServletResponse response) {
        try {
            return commonProductService.saveCommonProduct(commonProduct);
        } catch (IllegalArgumentException iae) {
            LOGGER.error("Validation failed", iae);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        } catch (Exception e) {
            LOGGER.error("Save products failed", e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }

    @RequestMapping(value="", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Secured("ROLE_ADMIN")
    public CommonProductsWrapper getCommonProductsByFilter(@RequestParam("page") Short page,
                                                           @RequestParam("id") Long id,
                                                           @RequestParam("name") String name,
                                                           @RequestParam("category_id") Long categoryId,
                                                           @RequestParam("sub_category_id") Long subCategoryId) {
        return commonProductService.getCommonProductsByFilter(id, name, categoryId, subCategoryId, page);
    }
}
