package com.shop.repository.delivery;

import com.shop.entity.delivery.DeliveryZone;
import com.shop.entity.delivery.DeliveryZoneTime;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalTime;

@Repository
public interface DeliveryZoneTimeRepository extends CrudRepository<DeliveryZoneTime, Long> {
    DeliveryZoneTime findByDeliveryZoneAndAfterAndBefore(DeliveryZone deliveryZone, LocalTime after, LocalTime before);
}
