package com.shop.repository.delivery;

import com.shop.entity.Status;
import com.shop.entity.delivery.DeliveryPoint;
import com.shop.entity.delivery.DeliveryZoneTime;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Set;

@Repository
public interface DeliveryPointRepository extends CrudRepository<DeliveryPoint, Long> {
    @EntityGraph(attributePaths = {"customerOrder", "deliveryZoneTime"})
    Set<DeliveryPoint> findAllByDeliveryDayInAndCustomerOrder_StatusNotInAndDeliveryZoneTimeIsNotNull(Set<LocalDate> deliveryDays, Set<Status.Customer> statuses);

    @EntityGraph(attributePaths = {"customerOrder.address", "deliveryZoneTime.deliveryZone.stock"})
    Set<DeliveryPoint> findAllByDeliveryDayAndCustomerOrder_StatusNotInAndDeliveryZoneTimeIsNotNull(LocalDate deliveryDay, Set<Status.Customer> statuses);

    @EntityGraph(attributePaths = {"customerOrder.address"})
    Set<DeliveryPoint> findAllByDeliveryDayAndDeliveryZoneTimeAndCustomerOrder_StatusNotIn(LocalDate deliveryDay, DeliveryZoneTime deliveryTime, Set<Status.Customer> statuses);
}
