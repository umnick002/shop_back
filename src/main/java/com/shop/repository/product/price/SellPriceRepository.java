package com.shop.repository.product.price;

import com.shop.entity.product.price.SellPrice;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface SellPriceRepository extends CrudRepository<SellPrice, Long> {
    @EntityGraph(attributePaths = {
            "product.commonProduct.contents",
            "product.commonProduct.subCategory.category",
            "product.sellPrices",
            "product.buyPrices",
    })
    SellPrice findTopByProductIdOrderByCreatedDesc(Long productId);

    Set<SellPrice> findByProductIdInOrderByCreatedDesc(Set<Long> productIds);
}
