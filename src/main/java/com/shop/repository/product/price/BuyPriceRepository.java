package com.shop.repository.product.price;

import com.shop.entity.product.price.BuyPrice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface BuyPriceRepository extends CrudRepository<BuyPrice, Long> {
    Set<BuyPrice> findByProductIdInOrderByCreatedDesc(Set<Long> productIds);
    BuyPrice findTopById(Long id);
}
