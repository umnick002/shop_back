package com.shop.repository.product.description;

import com.shop.entity.customer.CommonProductComment;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;


public interface CommonProductCommentRepository extends CrudRepository<CommonProductComment, Long> {
    CommonProductComment findByCustomerIdAndCommonProductId(Long customerId, Long commonProductId);

    @EntityGraph(attributePaths = {"customer"})
    Set<CommonProductComment> findAllByCommonProductId(Long commonProductId);

    @Query("select rating from CommonProductComment where common_product_id=:commonProductId")
    List<Short> getRatingByCommonProductId(@Param("commonProductId") Long commonProductId);
}
