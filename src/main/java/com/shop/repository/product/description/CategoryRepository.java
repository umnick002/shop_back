package com.shop.repository.product.description;

import com.shop.entity.product.description.Category;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

    @EntityGraph(attributePaths = {"subCategories"})
    Category findSubCategoriesById(Long id);

    Set<Category> findAllByOrderByPriorityAsc();

    @EntityGraph(attributePaths = "subCategories")
    Set<Category> getAllByOrderByPriorityAsc();
}
