package com.shop.repository.product.description;

import com.shop.entity.product.description.PropertyDictionary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyDictionaryRepository extends CrudRepository<PropertyDictionary, Long> {
}
