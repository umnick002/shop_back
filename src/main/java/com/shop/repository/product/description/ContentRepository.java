package com.shop.repository.product.description;

import com.shop.entity.product.description.Content;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ContentRepository extends CrudRepository<Content, Long> {
    @Query("select coalesce(max(c.priority), 0) from Content c where c.commonProductId=:commonProductId")
    Short findMaxPriorityByCommonProductId(@Param("commonProductId") Long commonProductId);

    Content findByHash(byte[] hash);
}
