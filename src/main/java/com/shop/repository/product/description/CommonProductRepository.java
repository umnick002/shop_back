package com.shop.repository.product.description;

import com.shop.entity.product.description.CommonProduct;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Repository
public interface CommonProductRepository extends PagingAndSortingRepository<CommonProduct, Long> {

    @EntityGraph(attributePaths = {
            "subCategory.category", "properties.key", "contents",
            "products.sellPrices", "comments.customer"
    })
    CommonProduct findCommonProductById(Long id);

    @EntityGraph(attributePaths = {
            "subCategory", "properties.key", "contents", "products"
    })
    CommonProduct getCommonProductById(Long id);

    @EntityGraph(attributePaths = {
            "properties.key", "contents", "products.sellPrices"
    })
    @Query("select cp from CommonProduct as cp where cp.subCategory.id in " +
            "(select sc.id from SubCategory as sc where sc.category.id=:id)")
    Set<CommonProduct> findAllByCategory_Id(@Param("id") Long id);

    @EntityGraph(attributePaths = {
            "properties.key", "contents", "products.sellPrices"
    })
    Set<CommonProduct> findAllBySubCategory_Id(Long secondaryId);

    @Modifying
    @Transactional
    @Query("update CommonProduct set rating=:rating where id=:id")
    void updateRating(@Param("id") Long id, @Param("rating") Double rating);
}
