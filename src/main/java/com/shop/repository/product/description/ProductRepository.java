package com.shop.repository.product.description;

import com.shop.entity.product.description.Product;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    @EntityGraph(attributePaths =
            {"buyPrice.stockOrderItems", "sellPrice.customerOrderItems"})
    Product findProductById(Long id);
}
