package com.shop.repository.customer;

import com.shop.entity.Status;
import com.shop.entity.customer.Customer;
import com.shop.entity.customer.CustomerAddress;
import com.shop.entity.customer.CustomerOrder;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface CustomerOrderRepository extends CrudRepository<CustomerOrder, Long> {
    @EntityGraph(attributePaths = {
            "customerOrderItems.sellPrice.product.commonProduct.contents",
            "customerOrderItems.sellPrice.product.commonProduct.subCategory.category",
            "customerOrderItems.sellPrice.product.sellPrices",
    })
    CustomerOrder findByCustomerAndStatus(@Param("customer") Customer customer, @Param("status") Status.Customer status);

    @EntityGraph(attributePaths = {
            "deliveryPoint.deliveryZoneTime", "address", "stock"
    })
    CustomerOrder getByCustomerAndStatus(@Param("customer") Customer customer, @Param("status") Status.Customer status);

    @EntityGraph(attributePaths = {
            "deliveryPoint", "address.deliveryZone"
    })
    CustomerOrder getTopByCustomerAndStatus(@Param("customer") Customer customer, @Param("status") Status.Customer status);

    @EntityGraph(attributePaths = {"customerOrderItems", "customer"})
    List<CustomerOrder> findAllByStatusInOrderByUpdatedDesc(Set<Status.Customer> statuses);

    @EntityGraph(attributePaths = {"customerOrderItems.sellPrice.product.commonProduct.contents"})
    CustomerOrder findCustomerOrderById(Long id);

    Set<CustomerOrder> findAllByCustomerAndAddressAndStatusNot(Customer customer, CustomerAddress address, Status.Customer status);
}
