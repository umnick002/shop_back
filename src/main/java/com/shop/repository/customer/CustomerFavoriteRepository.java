package com.shop.repository.customer;

import com.shop.entity.customer.CustomerFavorite;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerFavoriteRepository extends CrudRepository<CustomerFavorite, CustomerFavorite> {
    @Query("select cf from CustomerFavorite cf where customerId = (select id from Customer where username = :username) and favoriteId = :commonProductId")
    CustomerFavorite getFavoriteProduct(@Param("username") String customerName, @Param("commonProductId") Long commonProductId);
}
