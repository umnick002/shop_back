package com.shop.repository.customer;

import com.shop.entity.customer.Customer;
import com.shop.entity.customer.CustomerAddress;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CustomerAddressRepository extends CrudRepository<CustomerAddress, Long> {
    Set<CustomerAddress> getAllByCustomerAndRetiredFalseOrderByUpdatedDesc(Customer customer);

    CustomerAddress findByCustomerAndId(Customer customer, Long id);

    @EntityGraph(attributePaths = {"deliveryZone.days"})
    CustomerAddress getByCustomerAndId(Customer customer, Long id);

    @EntityGraph(attributePaths = {"deliveryZone.deliveryZoneTimes", "deliveryZone.days"})
    CustomerAddress getFirstByCustomerAndId(Customer customer, Long id);
}
