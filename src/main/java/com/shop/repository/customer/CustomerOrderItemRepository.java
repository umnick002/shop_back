package com.shop.repository.customer;

import com.shop.entity.customer.CustomerOrderItem;
import com.shop.entity.product.price.SellPrice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CustomerOrderItemRepository extends CrudRepository<CustomerOrderItem, Long> {
    Set<CustomerOrderItem> findAllBySellPriceIn(Set<SellPrice> sellPrices);
}
