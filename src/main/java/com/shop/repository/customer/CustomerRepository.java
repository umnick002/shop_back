package com.shop.repository.customer;

import com.shop.entity.customer.Customer;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    Customer findByUsername(String username);

    @EntityGraph(attributePaths = {"favorites"})
    Customer getByUsername(@Param("username") String username);
}
