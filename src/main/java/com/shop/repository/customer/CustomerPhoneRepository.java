package com.shop.repository.customer;

import com.shop.entity.customer.Customer;
import com.shop.entity.customer.CustomerPhone;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CustomerPhoneRepository extends CrudRepository<CustomerPhone, Long> {
    CustomerPhone findByCustomerAndPhone(Customer customer, String phone);
    CustomerPhone findByCustomerAndPhoneAndCode(Customer customer, String phone, String code);
    Set<CustomerPhone> findAllByCustomerAndVerifiedOrderByCreatedDesc(Customer customer, Boolean verified);
}
