package com.shop.repository.user;

import com.shop.entity.user.Group;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends CrudRepository<Group, Long> {
    Group findByName(String name);
    void deleteByName(String name);
}
