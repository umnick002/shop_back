package com.shop.repository.user;

import com.shop.entity.user.UserData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserData, Long> {
    UserData findByUsername(String username);
    Boolean existsByUsername(String username);
    void deleteByUsername(String username);
}
