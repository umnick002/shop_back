package com.shop.repository.user;

import com.shop.entity.user.PersistentLogin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersistentLoginRepository extends CrudRepository<PersistentLogin, String> {
    void deleteByUsername(String username);
}
