package com.shop.repository.stock;

import com.shop.entity.Status;
import com.shop.entity.stock.StockOrder;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface StockOrderRepository extends CrudRepository<StockOrder, Long> {

    @EntityGraph(attributePaths = {"stockOrderItems.buyPrice.product.commonProduct.contents"})
    StockOrder findStockOrderById(Long id);

    @EntityGraph(attributePaths = {"stockOrderItems"})
    List<StockOrder> findAllByStatusInOrderByUpdatedDesc(Set<Status.Stock> statuses);

    StockOrder findByStatusAndStockId(Status.Stock status, Long stockId);
}
