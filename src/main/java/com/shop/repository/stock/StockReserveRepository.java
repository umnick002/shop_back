package com.shop.repository.stock;

import com.shop.entity.stock.StockReserve;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Set;

@Repository
public interface StockReserveRepository extends CrudRepository<StockReserve, Long> {
    StockReserve findByProductId(Long productId);

    @Query("select sr from StockReserve sr where sr.productId in :productIds and (sr.reserve > 0 or sr.updated > :updated)")
    Set<StockReserve> findReservedProducts(@Param("productIds") Set<Long> productIds, @Param("updated") LocalDateTime updated);
}
