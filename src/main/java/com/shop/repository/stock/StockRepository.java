package com.shop.repository.stock;

import com.shop.entity.stock.Stock;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface StockRepository extends CrudRepository<Stock, Long> {
    @EntityGraph(attributePaths = "deliveryZones")
    Set<Stock> findAll();
}
