package com.shop.repository.stock;

import com.shop.entity.stock.StockOrderItem;
import com.shop.entity.product.price.BuyPrice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface StockOrderItemRepository extends CrudRepository<StockOrderItem, Long> {
    Set<StockOrderItem> findAllByBuyPriceIn(Set<BuyPrice> buyPrices);
}
