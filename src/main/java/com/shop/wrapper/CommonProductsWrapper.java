package com.shop.wrapper;

import com.shop.entity.product.description.CommonProduct;

import java.util.List;

public class CommonProductsWrapper {

    private List<CommonProduct> commonProducts;
    private Integer pages;

    public void setCommonProducts(List<CommonProduct> commonProducts) {
        this.commonProducts = commonProducts;
    }

    public List<CommonProduct> getCommonProducts() {
        return commonProducts;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }
}
